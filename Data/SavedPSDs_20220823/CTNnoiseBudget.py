import numpy as np
from uncertainties import ufloat as uf
from uncertainties import unumpy as unp
from noiseBudgetModule import noiseBudget
import argparse
import yaml

import time

def main(args):
    dataFiles = {}
    if args.dataFiles is not None:
        with open(args.dataFiles,'r') as df:
            dataFiles = yaml.full_load(df)
    defaultNoneKeys = ['pllOsc', 'Seismic', 'RINfiles', 'FSS_OLTF', 'pllRead']
    for key in defaultNoneKeys:
        if key not in list(dataFiles.keys()):
            dataFiles[key] = None

    start = time.time()

    nosbud = noiseBudget(params=args.paramFile)
    nosbud.freq = np.loadtxt('frequencies.txt')

    if dataFiles['FSS_OLTF'] is not None:
        ff = nosbud.freq

        # Start of data loading and calculation code.
        # NPRO free-running frequency noise ASD:
        #                Willke et al., Opt. Lett. vol 25 no 14
        # PDH OLTFs: ctn:1504
        nproFreeASD = 10**4/ff # Hz/Hz**0.5
        print('Using {fn1}\n  and {fn2}\n      for Residual NPRO '
              'Noise Calculation.'.format(fn1=dataFiles['FSS_OLTF'][0],
                                          fn2=dataFiles['FSS_OLTF'][1]))

        # Load vector-fitted versions of PDH OLTF data
        NorthTFData = np.loadtxt(dataFiles['FSS_OLTF'][0])
        SouthTFData = np.loadtxt(dataFiles['FSS_OLTF'][1])
        NorthOLTFMag = np.interp(ff, NorthTFData[:, 0], NorthTFData[:, 1])
        SouthOLTFMag = np.interp(ff, SouthTFData[:, 0], SouthTFData[:, 1])
        
        northResidFreqPSD = (nproFreeASD / NorthOLTFMag)**2
        southResidFreqPSD = (nproFreeASD / SouthOLTFMag)**2
        residFreqPSD = northResidFreqPSD + southResidFreqPSD
        # End of data loading and calculation code

        nosbud.PSDList['resNPRO'] = [residFreqPSD , ff, 
                                     'Residual NPRO noise'];

    if dataFiles['RINfiles'] is not None:
        if isinstance(dataFiles['RINfiles'], list):
            print('Using {fn1}\n  and {fn2}\n      for Photothermal '
                  'Noise Calculation'.format(fn1=dataFiles['RINfiles'][0],
                                             fn2=dataFiles['RINfiles'][1]))
        else:
            print('Using {fn} for Photothermal '
                  'Noise Calculation.'.format(fn=dataFiles['RINfiles']))
        coatAbs = uf(6,1)*1e-6   #From previous notebook
        nosbud.calculatePhotoThermalNoise(dataFiles['RINfiles'], coatAbs)

    nosbud.calculateCoatingBrownianNoise()

    nosbud.calculateCoatingThermoOpticNoise()

    nosbud.calculateSubstrateBrownianNoise()

    nosbud.calculateSubstrateThermoElasticNoise()

    nosbud.calculatePDHShotNoise()

    if dataFiles['pllOsc'] is not None:
        # PLL Oscillation Noise
        ff = nosbud.freq

        print('Using {fn}\n      for PLL Oscillation '
              'Noise.'.format(fn=dataFiles['pllOsc']))
        #Start of data loading code
        pllOscData = np.loadtxt(dataFiles['pllOsc'])
        pllOscFreq = pllOscData[:,0]
        pllOscASD = pllOscData[:,1]
        pllOscPSDlb = np.interp(ff, pllOscFreq, pllOscData[:,2]**2)
        pllOscPSDub = np.interp(ff, pllOscFreq, pllOscData[:,3]**2)
        pllOscPSD = np.interp(ff, pllOscFreq, pllOscASD**2)
        #End of data laoding code

        nosbud.PSDList['pllOsc'] = [pllOscPSD, ff,
                                    'Moku Frequency Noise',
                                    pllOscPSDlb, pllOscPSDub]

    # PLL Readout Noise
    if dataFiles['pllRead'] is not None:
        ff = nosbud.freq
        pllReadoutNoiseData = np.loadtxt(dataFiles['pllRead'])
        pllReadPSD = np.interp(ff, pllReadoutNoiseData[:, 0],
                               pllReadoutNoiseData[:, 1]**2)
        nosbud.PSDList['pllReadout'] = [pllReadPSD, ff,
                                        'PLL Electronic Reaodut Noise' ]

    if dataFiles['Seismic'] is not None:
        # Seismic Noise
        ff = nosbud.freq
        cavLen = nosbud.cavLen
        fConv = nosbud.fConv

        # Start of data loading and calculation code.
        import scipy.io as scio
        def mechTF(ff, f0, Q):
            return 1/(1+1j*ff/(f0*Q)-(ff/f0)**2)
        # from a Guralp
        print('Using {fn}\n      for Seismic '
              'Noise Calculation.'.format(fn=dataFiles['Seismic']))
        seisData = scio.loadmat(dataFiles['Seismic'])
        # unfloated vertical velocity, m/(s * Hz**0.5)
        seisFreq, seisVel = seisData['f_ver'][:,0], seisData['uf_ver'][:,2]
        seisAcc = np.interp(ff, seisFreq, seisVel * 2*np.pi*seisFreq)
        fStack1 = 10.7 # Hz
        QStack1 = 15
        fStack2 = 35.4 # Hz
        QStack2 = 8
        fSpring = 5.2 # Hz
        QSpring = 10
        stackTF = mechTF(ff, fStack1, QStack1) * mechTF(ff, fStack2, QStack2)
        springTF = mechTF(ff, fSpring, QSpring)
        seisTF = stackTF*springTF
        seisCouple = 6e-12 * cavLen # m / (m s**-2)
        seismicPSD = (seisAcc * np.abs(seisTF))**2
        seismicPSD = (unp.uarray(seismicPSD, seismicPSD/50**0.5)
                      * seisCouple**2 * fConv**2) # Hz/sqrt(Hz)
        # End of data loading and calculation code

        nosbud.PSDList['seismic'] = [seismicPSD, ff, 'Seismic Noise' ]

    print('All calculations finished.')

    print(time.time()-start, 's Elapsed.')
    print('Saving data...')
    # Save all PSD data
    nosbud.savePSD(saveList='all', filename = args.filename, savePTfac=True);
    print(time.time()-start, 's Elapsed.')

def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='This script runs noiseBudget calculations for a given '
                    'noise budget configuration file, coating structure file '
                    'and other required files.')
    group = parser.add_mutually_exclusive_group()

    group.add_argument('paramFile', nargs='?',
                       help='The parameter file for the budget',
                       default=None)

    parser.add_argument('--dataFiles', help='Data files for some '
                        'noise contributions',
                         default=None)

    parser.add_argument('-f', '--filename', help='Stem of output filename'
                        'where PSD data will be saved.', default=None)
    return parser.parse_args()

if __name__ == "__main__":
    args = grabInputArgs()
    main(args)
