'''
dailyBNspec
This script will take a beatnote timeseries measurement usng Moku on
triggerTime everyday. Then it will run mokuReadFreqNoise and will also create
a log file containing experiment configuration details in yaml format.
Finally, it will git add the three files generated and commit and push them.
'''

from BNSpec import BNspec
from gitCommitAndPush import gitCommitAndPush
from beatnoteSpecPlot import beatnoteSpecPlot
import argparse
import time
import os

dataDir = ('/home/controls/Git/cit_ctnlab/ctn_noisebudget/'
           + 'Data/dailyBeatNoteData/')


def main(args):
    cwd = os.getcwd()
    while(1):
        timeStr = time.strftime('%H:%M', time.localtime())
        if timeStr == args.triggerTime or args.triggerNow:
            print('Triggered at',
                  time.strftime('%H:%M', time.localtime()),
                  '.Taking beatnote timeseries measurement...')
            os.chdir(dataDir)
            fileList = BNspec(detector=args.detector,
                              instrument=args.instrument,
                              fileRoot='Beatnote', ipAddress=args.ipAddress,
                              duration=args.duration,
                              sampleRate=args.sampleRate,
                              bandWidth=args.bandWidth, atten=args.atten,
                              paramFile=args.paramFile, measRIN=args.measRIN)
            if args.saveSpace:
                os.remove(fileList[0])
            os.chdir(cwd)
            if fileList[0] != 'Error':
                if not args.testing:
                    # Correct path for data files
                    for ii, file in enumerate(fileList):
                        fileList[ii] = dataDir + file
                    # Plot the spectrum
                    fn1, fn2 = beatnoteSpecPlot(
                                            filesDir=dataDir,
                                            savedEstPSDs=args.savedEstPSDs,
                                            unverified=True)
                    fileList += [fn1, fn2]
                    # Push the files to git repo
                    cm = ('Beatnote '
                          + time.strftime('%b %d %Y - %H:%M:%S',
                                          time.localtime())
                          + ' added automatically')
                    gitCommitAndPush(fileList=fileList,
                                     commitMessage=cm,
                                     branchName='master')
                if args.triggerNow or args.testing:
                    break
                # Sleep for 61 seconds to avoid re-trigger
                time.sleep(61)
                print('Done! See you in', args.repeat, 'Day(s) :)')
                # Wait for the trigger day to come.
                time.sleep(24*3600*(args.repeat-1))
                # Measurement was not succesful. Wait for 5s and try again.
            if args.triggerNow or args.testing:
                break


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='This script runs moku to take beatnote measurement at '
                    'a specified time everydau. It then runs mokuReadFreqNoise'
                    'on frequency time series. In addition, it also reads '
                    'channels written in ChannelList file and logs them in '
                    'a log file. Detector information is also written on '
                    'the log file.')
    parser.add_argument('-t', '--triggerTime', type=str,
                        help='hh:mm string when beatnote measurement will '
                             'be triggered',
                        default='03:00')
    parser.add_argument('--instrument', type=str,
                        help='Moku or SR785',
                        default='Moku')
    parser.add_argument('-i', '--ipAddress',
                        help='IP address of Moku or SR785',
                        default='10.0.1.81')
    parser.add_argument('-b', '--bandWidth', type=float,
                        help='Phasemeter tracking bandwidth.'
                             'Default is 10kHz.',
                        default=10e3)
    parser.add_argument('--duration', type=int,
                        help='Measurement time in seconds. Default 60.',
                        default=60)
    parser.add_argument('--sampleRate', type=str,
                        help='Sampling Rate. {veryslow, slow , medium ,'
                             'fast, veryfast, ultrafast}. Refer moku manual.'
                             'Default is veryfast.',
                        default='veryfast')
    parser.add_argument('-a', '--atten',
                        help='10x Attenuation. True: input range of 10Vpp.'
                             'False: inpute range of 1Vpp. Default: False',
                        action='store_true')
    parser.add_argument('-v', '--verbose',
                        help='Will diplay some calculation parameters.',
                        action='store_true')
    parser.add_argument('--detector', type=str,
                        help='Detector used. NF1811 or SN101(default)',
                        default='SN101')
    parser.add_argument('--savedEstPSDs', type=str,
                        help='File where saved estimates of noise '
                             'contributions are present.',
                        default=None)
    parser.add_argument('--testing',
                        help='Would only test moku functionality.',
                        action='store_true')
    parser.add_argument('--triggerNow',
                        help='Would trigger once and exit.',
                        action='store_true')
    parser.add_argument('--paramFile', nargs='?',
                        help='The parameter file for the measurement by SR785',
                        default='CTN_BNSpec_SR785.yml')
    parser.add_argument('-r', '--repeat', type=int,
                        help='Repeat every _ day(s). Default 1.',
                        default=1)
    parser.add_argument('--saveSpace',
                        help='Would only test moku functionality.',
                        action='store_true')
    parser.add_argument('--measRIN',
                        help='Measure transmission RIN after beatnote '
                             'measurement.',
                        action='store_true')
    return parser.parse_args()


if __name__ == "__main__":
    args = grabInputArgs()
    args.instrument = args.instrument.lower()
    if args.instrument != 'moku' and args.instrument != 'sr785':
        raise RuntimeError('Instrument can be Moku or SR785 only.')
    main(args)
