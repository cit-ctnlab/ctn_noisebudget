'''
dailyBNcp
This script will copy everyday latest beatnote measurement files from any
directory where measurements are happening. It is assumed that measurements
are kept at DataMMDD directory in the directory provided.
It will plot the latest
measurements like dailyBNspec.
Finally, it will git add the files copied/generated and commit and push them.
'''

from gitCommitAndPush import gitCommitAndPush
from beatnoteSpecPlot import beatnoteSpecPlot
import argparse
import time
import os
import shutil

dailyDataDir = ('/home/controls/Git/cit_ctnlab/ctn_noisebudget/'
                + 'Data/dailyBeatNoteData/')


def main(args):
    cwd = os.getcwd()
    while(1):
        timeStr = time.strftime('%H:%M', time.localtime())
        if timeStr == args.triggerTime or args.triggerNow:
            print('Triggered at',
                  time.strftime('%H:%M', time.localtime()),
                  '. Copying latest beatnote measurement...')
            todayDataDir = os.path.join(args.dataDir,
                                        time.strftime('Data%m%d/'))
            tm_mday = time.localtime().tm_mday
            while not os.path.exists(todayDataDir):
                # If today's data directory does not exist
                time.sleep(3600) # Check every hour for it
                if tm_mday != time.localtime().tm_mday:
                    print('No new measurements today.')
                    break
            if not os.path.exists(todayDataDir):
                continue
            os.chdir(todayDataDir)
            fl = [fn for fn in os.listdir() if fn.find('.yml') != -1]
            fl.sort(key=fnTomktime)
            fileList = [fl[-1]]
            fileList += [fl[-1].replace('ExpConfig',
                                        'Spectrum').replace('.yml', '.txt')]
            possTransRINfile = fl[-1].replace('ExpConfig',
                                              'TransRIN').replace('.yml',
                                                                  '.txt')
            if os.path.isfile(possTransRINfile):
                fileList += [possTransRINfile]
            for ii, file in enumerate(fileList):
                print('Copying', file, 'to', dailyDataDir)
                shutil.copy2(file, dailyDataDir)
                fileList[ii] = dailyDataDir+file
            os.chdir(cwd)
            if not args.testing:
                # Plot the spectrum
                fn1, fn2 = beatnoteSpecPlot(
                                        filesDir=dailyDataDir,
                                        savedEstPSDs=args.savedEstPSDs,
                                        unverified=True)
                fileList += [fn1, fn2]
                # Push the files to git repo
                cm = ('Beatnote '
                      + time.strftime('%b %d %Y - %H:%M:%S',
                                      time.localtime())
                      + ' added automatically')
                gitCommitAndPush(fileList=fileList,
                                 commitMessage=cm,
                                 branchName='master')
            if args.triggerNow or args.testing:
                break
            # Sleep for 61 seconds to avoid re-trigger
            time.sleep(61)
            print('Done! See you in', args.repeat, 'Day(s) :)')
            # Wait for the trigger day to come.
            time.sleep(24*3600*(args.repeat-1))


def fnTomktime(fn):
    return time.mktime(fnToTstruc(fn))


def fnToTstruc(fn):
    tstruc = time.strptime(fn, 'BeatnoteExpConfig_%Y%m%d_%H%M%S.yml')
    return tstruc


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='This script copies the  beatnote measurement at '
                    'a specified time everyday from data directory where'
                    'measurements are happening.')
    parser.add_argument('dataDir', type=str,
                        help='Directory where measurements are happening')
    parser.add_argument('-t', '--triggerTime', type=str,
                        help='hh:mm string when beatnote measurement will '
                             'be triggered',
                        default='03:00')
    parser.add_argument('-v', '--verbose',
                        help='Will diplay some calculation parameters.',
                        action='store_true')
    parser.add_argument('--savedEstPSDs', type=str,
                        help='File where saved estimates of noise '
                             'contributions are present.',
                        default=None)
    parser.add_argument('--testing',
                        help='Would only test coping functionality.',
                        action='store_true')
    parser.add_argument('--triggerNow',
                        help='Would trigger once and exit.',
                        action='store_true')
    parser.add_argument('-r', '--repeat', type=int,
                        help='Repeat every _ day(s). Default 1.',
                        default=1)
    return parser.parse_args()


if __name__ == "__main__":
    args = grabInputArgs()
    main(args)
