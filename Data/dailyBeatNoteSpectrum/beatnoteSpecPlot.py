'''
beatnoteSpecPlot
This script will plot all daily beatnote spectrums with alpha scale to see
movement of spectrum over days.
'''

import argparse
import time
import yaml
import os
import numpy as np
from noiseBudgetModule import noiseBudget
from uncertainties import ufloat as uf
from uncertainties import unumpy as unp
import matplotlib.pyplot as plt  # For plotting
import matplotlib
matplotlib.use("Agg")
# *****************************************************************************
# Setting RC Parameters for figure size and fontsizes
import matplotlib.pylab as pylab
params = {'legend.fontsize': 'xx-large',
          'figure.figsize': (20, 10),
          'axes.labelsize': 'xx-large',
          'axes.titlesize': 'xx-large',
          'xtick.labelsize': 'xx-large',
          'ytick.labelsize': 'xx-large'}
pylab.rcParams.update(params)


def beatnoteSpecPlot(filesDir=None, savedEstPSDs=None,
                     dailyPlotName='CTN_Daily_BN_Spec.pdf',
                     latestPlotName='CTN_Latest_BN_Spec.pdf',
                     oldEstASDs=None, unverified=False):
    cwd = os.getcwd()

    if savedEstPSDs is not None:
        try:
            dirName = os.path.dirname(savedEstPSDs)
            for fn in os.listdir(dirName):
                if fn.find('Noise_Budget') != -1 and fn.find('.yml') != -1:
                    paramFile = fn
            os.chdir(dirName)
            nosbud = noiseBudget(params=paramFile, lightInit=True)
            os.chdir(cwd)
        except BaseException as e:
            print('Did not find any associated parameter file.')
            nosbud = noiseBudget(lightInit=True)
    else:
        nosbud = noiseBudget(lightInit=True)

    if filesDir is not None:
        os.chdir(filesDir)

    fl = os.listdir()
    BNspecList = []
    for fn in fl:
        if fn.find('BeatnoteSpectrum_') == 0:
            cn = fn.replace('Spectrum', 'ExpConfig')
            cn = cn.replace('.txt', '.yml')
            with open(cn, 'r') as p:
                temp = yaml.full_load(p)
            try:
                inst = temp['instrument']
            except BaseException:
                inst = 'Moku'
            if fn.replace('Spectrum', 'TransRIN') in fl:
                tn = fn.replace('Spectrum', 'TransRIN')
            else:
                tn = None
            BNspecList += [{'filename': fn,
                            'data': np.loadtxt(fn),
                            'detector': temp['detector'],
                            'instrument': inst,
                            'transRIN': tn}]
    BNspecList.sort(key=fnTomktime)
    toPlot = []

    if oldEstASDs is not None:
        for fn in oldEstASDs:
            if fn.rfind('/') != -1:
                label = fn[fn.rfind('/')+1:].replace('.txt', '')
            if fn.rfind('\\') != -1:
                label = fn[fn.rfind('\\')+1:].replace('.txt', '')
            else:
                label = fn
            data = np.loadtxt(fn)
            nosbud.PSDList[label] = [unp.uarray(data[:, 1]**2, data[:, 2]**2),
                                     data[:, 0], label]
            toPlot += [label]

    asdPlot1Title = ('CIT CTN Daily Beatnote Spectrum\n'
                     + 'Red/Magenta: Taken by SN101,'
                     + 'Blue/Cyan: Taken by NF1811\n'
                     + 'Opaque -> Transparent: Latest -> Oldest\n'
                     + 'Linestyles: SR785 -.    ;    Moku -')
    asdPlot2Title = 'CIT CTN Latest Beatnote Spectrums'
    asdPlot1Name = dailyPlotName
    asdPlot2Name = latestPlotName
    if savedEstPSDs is not None:
        nosbud.loadPSD(savedEstPSDs, overridePresentFreq=True)
        toPlot += ['coatBr', 'coatTO', 'subBr', 'subTE',
                   'pdhShot', 'pllOsc', 'pllReadout', 'seismic',
                   'photoThermal', 'resNPRO', 'total']
        asdPlot1 = nosbud.plotPSD(plotList=toPlot,
                                  savePlot=False,
                                  doTotal=False)
        ax = asdPlot1.axes[0]
        asdPlot1Title += '\nUsing ' + savedEstPSDs
        asdPlot2Title += '\nUsing ' + savedEstPSDs
    else:
        asdPlot1 = plt.figure(figsize=(16, 12))
        ax = asdPlot1.add_subplot(111)

    alphaScale = np.logspace(np.log10(0.2), 0, len(BNspecList))
    for ii, val in enumerate(BNspecList):
        data = val['data']
        ff = data[:, 0]
        ASD = data[:, 1]
        if val['detector'] == 'SN101':
            color = 'm'
            daysColor = 'r'
        else:
            color = 'c'
            daysColor = 'b'
        if val['instrument'] == 'SR785':
            ls = '-.'
        else:
            ls = '-'
        if ii < len(BNspecList)-1:
            ax.loglog(ff, ASD, alpha=alphaScale[ii], c=color, lw=1.5, ls=ls)
        else:
            ax.loglog(ff, ASD, alpha=alphaScale[ii], c=daysColor, lw=1.5,
                      label=fnToLabel(val['filename']), ls=ls)
    ax.set_xlabel('Frequency [Hz]')
    ax.set_ylabel(r'ASD of beat note frequency $[Hz/Hz^{1/2}]$')
    ax.legend(loc='best', fontsize=18, numpoints=2, borderpad=0.1,
              ncol=2, columnspacing=0.5, framealpha=0.7)
    ax.grid(linestyle='solid', which='major', alpha=0.75)
    ax.grid(linestyle='dashed', which='minor', alpha=0.25)
    ylimits = [6e-5, 2e3]
    ax.set_ylim(ylimits[0], ylimits[1])
    xlimits = [1, 10e3]
    ax.set_xlim(xlimits[0], xlimits[1])
    ax.set_title(asdPlot1Title)
    if unverified:
        ax.text(100, 100,'UNVERIFIED', fontsize=50, color='gray', ha='center',
                va='bottom', alpha=0.5)
    for ind in range(-3,0):
        fn = BNspecList[ind]['filename']
        nosbud.loadASD(fn,
                       label = fnToLabel(fn),
                       key = 'latest'+str(-1*ind))
    toPlot += ['latest1', 'latest2', 'latest3']
    if BNspecList[-1]['transRIN'] is not None:
        if 'PT_wo_RIN_coatAbs' in nosbud.PSDList:
            RINdata = np.loadtxt(BNspecList[-1]['transRIN'])
            with open(BNspecList[-1]['transRIN'], 'r') as f:
                header = f.readline()
            temp = header[header.find('North DC Val:')+13:].replace(' ', '')
            NDC = float(temp.split('Volts')[0])
            temp = header[header.find('South DC Val:')+13:].replace(' ', '')
            SDC = float(temp.split('Volts')[0])
            cpb = int((np.shape(RINdata)[1]-1)/2)
            RINdata[:, 1:cpb+1] = RINdata[:, 1:cpb+1]/NDC
            RINdata[:, cpb+1:] = RINdata[:, cpb+1:]/SDC
            NFin = uf(16700, 1400)
            SFin = uf(15100, 340)
            PincN = 3.9 * NDC * 1e-3    # Using calibration from 3/20/20
            PincS = 4.005 * SDC * 1e-3  # Using calibration from 3/16/20
            PcircN = PincN * NFin / np.pi
            PcircS = PincS * SFin / np.pi
            print('Before correction incident power', nosbud.powInc*1e3, 'mW')
            print('Incident Power at North Cavity', PincN*1e3, 'mW')
            print('Incident Power at South Cavity', PincS*1e3, 'mW')
            nosbud.updatePhotoThermalNoise(RINdata, uf(6, 1) * 1e-6,
                                           Pcirc=[PcircN, PcircS])
            nosbud.PSDList['photoThermal'][2] = ('Photothermal Noise from\n'
                                                 + fnToRINLabel(fn))
            nosbud.updatePDHShotNoise([PincN, PincS])
    asdPlot2 = nosbud.plotPSD(plotList=toPlot,
                              title=asdPlot2Title,
                              doTotal=True,
                              savePlot=False)
    ax = asdPlot2.axes[0]
    ax.set_xlim(xlimits[0], xlimits[1])
    if unverified:
        ax.text(100, 100,'UNVERIFIED', fontsize=50, color='gray', ha='center',
                va='bottom', alpha=0.5)
    os.chdir(cwd)
    asdPlot1.savefig(asdPlot1Name,
                     facecolor=asdPlot1.get_facecolor(),
                     bbox_inches='tight')
    asdPlot2.savefig(asdPlot2Name,
                     facecolor=asdPlot2.get_facecolor(),
                     bbox_inches='tight')

    return asdPlot1Name, asdPlot2Name


def fnTomktime(ele):
    fn = ele['filename']
    fn = fn.replace('.txt', '')
    tstamp = fn[fn.find('_')+1:]
    tstruc = time.strptime(tstamp, '%Y%m%d_%H%M%S')
    return time.mktime(tstruc)


def fnToLabel(fn):
    fn = fn.replace('.txt', '')
    tstamp = fn[fn.find('_')+1:]
    tstruc = time.strptime(tstamp, '%Y%m%d_%H%M%S')
    return time.strftime('%b %d, %Y Beat', tstruc)

def fnToRINLabel(fn):
    fn = fn.replace('.txt', '')
    tstamp = fn[fn.find('_')+1:]
    tstruc = time.strptime(tstamp, '%Y%m%d_%H%M%S')
    return time.strftime('%b %d, %Y Trans RIN', tstruc)


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='This script will plot all daily beatnote spectrums with'
                    ' alpha scale to see movement of spectrum over days.')
    parser.add_argument('-d', '--filesDir', type=str,
                        help='Directory where beatnote files are.'
                             'Default is current directory.',
                        default=None)
    parser.add_argument('--savedEstPSDs', type=str,
                        help='File where saved estimates of noise '
                             'contributions are present.',
                        default=None)
    parser.add_argument('--oldEstASDs', type=str, nargs='+',
                        help='File where saved estimates of old calculation'
                             'of noise contributions are present.',
                        default=None)
    parser.add_argument('--dailyPlotName', type=str,
                        help='Filename of daily spectrum plot. '
                             'Default CTN_Daily_BN_Spec.pdf',
                        default='CTN_Daily_BN_Spec.pdf')
    parser.add_argument('--latestPlotName', type=str,
                        help='Filename of daily spectrum plot. '
                             'Default CTN_Latest_BN_Spec.pdf',
                        default='CTN_Latest_BN_Spec.pdf')
    parser.add_argument('--unverified',
                        help='Will put Unverified watermark. Default: False.',
                        action='store_true')
    return parser.parse_args()


if __name__ == "__main__":
    args = grabInputArgs()
    beatnoteSpecPlot(args.filesDir, args.savedEstPSDs,
                     dailyPlotName=args.dailyPlotName,
                     latestPlotName=args.latestPlotName,
                     oldEstASDs=args.oldEstASDs,
                     unverified=args.unverified)
