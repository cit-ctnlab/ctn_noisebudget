'''
dailyBNspec
This script will run mokuReadFreqNoise with same parameters and on top of that
will also create a log file containing experiment configuration details in
yaml format.
'''

from mokuReadFreqNoise import main as mokuReadFreqNoise
import argparse
import time
from epics import caget
import yaml
import os

channelListFile = 'ChannelList.txt'


def main(args):
    if args.saveFileName is None:
        if args.dataFileName is None:
            args.saveFileName = ('BeatnoteSpec_'
                                 + time.strftime("%Y%m%d_%H%M%S")
                                 + '.txt')
        elif args.dataFileName.find('/') != -1:
            saveFileName = args.dataFileName[args.dataFileName.rfind('/')+1:]
            args.saveFileName = saveFileName.replace('.csv', '.txt')
        elif args.dataFileName.find('\\') != -1:    # For Windows
            saveFileName = args.dataFileName[args.dataFileName.rfind('\\')+1:]
            args.saveFileName = saveFileName.replace('.csv', '.txt')
        else:
            args.saveFileName = args.dataFileName.replace('.csv', '.txt')
    if args.saveDir is None:
        args.saveDir = os.getcwd()+'/'
    if not args.skipMoku:
        mokuReadFreqNoise(args)
    writeLog(args)


def writeLog(args):
    if args.saveFileName is None:
        if args.dataFileName is None:
            logFileName = ('BeatnoteSpec_' + time.strftime("%Y%m%d_%H%M%S")
                           + '.yml')
        else:
            logFileName = args.dataFileName.replace('.csv', '.yml')
    else:
        logFileName = args.saveFileName.replace('.txt', '.yml')
    with open(channelListFile, 'r') as clf:
        chList = clf.readlines()

    dictToWrite = {}
    # Remove any CR or LF and then read the channels and store in a dict
    for ch in chList:
        ch = ch.replace('\n', '')
        ch = ch.replace('\r', '')
        value = caget(ch)
        dictToWrite[ch] = value

    # Add Detector
    dictToWrite['detector'] = args.detector
    with open(logFileName, 'w') as logFile:
        yaml.safe_dump(dictToWrite, logFile)


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='This script runs mokuReadFreqNoise on frequency data of '
                    'beatnote measured by moku. In addition, it also reads '
                    'channels written in ChannelList file and logs them in '
                    'a log file. Detector information is also written on '
                    'the log file.')
    parser.add_argument('-d', '--dataFileName', type=str,
                        help='Data file in csv format. Use liconvert to '
                             'convert .li file to  .csv',
                        default=None)
    parser.add_argument('-f', '--saveFileName', type=str,
                        help='File name to save with. You can also add path '
                             'to the filename here if different from data '
                             'file name. Default is data file with .txt',
                        default=None)
    parser.add_argument('-s', '--saveDir', type=str,
                        help='Directory address where to save generated file. '
                             'Default is current directory.',
                        default=None)
    parser.add_argument('-v', '--verbose',
                        help='Will diplay some calculation parameters.',
                        action='store_true')
    parser.add_argument('-k', '--skipMoku',
                        help='If True, mokuReadFreqNoise will not be run.',
                        action='store_true')
    parser.add_argument('--detector', type=str,
                        help='Detector used. NF1811 or SN101',
                        default='NF1811')
    return parser.parse_args()


if __name__ == "__main__":
    args = grabInputArgs()
    main(args)
