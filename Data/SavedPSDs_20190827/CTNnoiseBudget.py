import numpy as np
from uncertainties import ufloat as uf
from uncertainties import unumpy as unp
from noiseBudgetModule import noiseBudget
import argparse
import yaml

def main(args):
    dataFiles = {}
    if args.dataFiles is not None:
        with open(args.dataFiles,'r') as df:
            dataFiles = yaml.full_load(df)
    defaultNoneKeys = ['pllOsc', 'Seismic', 'RINfiles', 'pdhLTI']
    for key in defaultNoneKeys:
        if key not in list(dataFiles.keys()):
            dataFiles[key] = None

    nosbud = noiseBudget(params=args.paramFile)

    nosbud.calculateCoatingBrownianNoise()

    nosbud.calculateCoatingThermoOpticNoise()

    nosbud.calculateSubstrateBrownianNoise()

    nosbud.calculateSubstrateThermoElasticNoise()

    nosbud.calculatePDHShotNoise()

    if dataFiles['pllOsc'] is not None:
        # PLL Oscillation Noise
        ff = nosbud.freq

        print('Using {fn}\n      for PLL Oscillation '
              'Noise.'.format(fn=dataFiles['pllOsc']))
        #Start of data loading code
        pllOscFreq, pllOscASD = np.loadtxt(dataFiles['pllOsc'],
                                           unpack=1)
        pllOscPSD = np.interp(ff, pllOscFreq, pllOscASD**2)
        pllOscPSD = unp.uarray(pllOscPSD, pllOscPSD/50**0.5)
        #End of data laoding code

        nosbud.PSDList['pllOsc'] = [pllOscPSD, ff,
                                    'PLL Oscillation Noise @ 1 kHz/V' ]

    # PLL Readout Noise
    ff = nosbud.freq
    #Start of calculation code
    # Magic number from Tara's old noise budget; somehow connected to
    # elog 2012-02-09
    pllReadPSD = (ff*0.0207*5.04e-5)**2
    pllReadPSD = unp.uarray(pllReadPSD, pllReadPSD/50**0.5)
    #End of calculation code

    nosbud.PSDList['pllReadout'] = [pllReadPSD, ff,
                                    'PLL Electronic Reaodut Noise' ]

    if dataFiles['Seismic'] is not None:
        # Seismic Noise
        ff = nosbud.freq
        cavLen = nosbud.cavLen
        fConv = nosbud.fConv

        # Start of data loading and calculation code.
        import scipy.io as scio
        def mechTF(ff, f0, Q):
            return 1/(1+1j*ff/(f0*Q)-(ff/f0)**2)
        # from a Guralp
        print('Using {fn}\n      for Seismic '
              'Noise Calculation.'.format(fn=dataFiles['Seismic']))
        seisData = scio.loadmat(dataFiles['Seismic'])
        # unfloated vertical velocity, m/(s * Hz**0.5)
        seisFreq, seisVel = seisData['f_ver'][:,0], seisData['uf_ver'][:,2]
        seisAcc = np.interp(ff, seisFreq, seisVel * 2*np.pi*seisFreq)
        fStack1 = 10.7 # Hz
        QStack1 = 15
        fStack2 = 35.4 # Hz
        QStack2 = 8
        fSpring = 5.2 # Hz
        QSpring = 10
        stackTF = mechTF(ff, fStack1, QStack1) * mechTF(ff, fStack2, QStack2)
        springTF = mechTF(ff, fSpring, QSpring)
        seisTF = stackTF*springTF
        seisCouple = 6e-12 * cavLen # m / (m s**-2)
        seismicPSD = (seisAcc * np.abs(seisTF))**2
        seismicPSD = (unp.uarray(seismicPSD, seismicPSD/50**0.5)
                      * seisCouple**2 * fConv**2) # Hz/sqrt(Hz)
        # End of data loading and calculation code

        nosbud.PSDList['seismic'] = [seismicPSD, ff, 'Seismic Noise' ]

    if dataFiles['RINfiles'] is not None:
        if isinstance(dataFiles['RINfiles'], list):
            print('Using {fn1}\n  and {fn2}\n      for Photothermal '
                  'Noise Calculation'.format(fn1=dataFiles['RINfiles'][0],
                                             fn2=dataFiles['RINfiles'][1]))
        else:
            print('Using {fn} for Photothermal '
                  'Noise Calculation.'.format(fn=dataFiles['RINfiles']))
        coatAbs = uf(6,1)*1e-6   #From previous notebook
        nosbud.calculatePhotoThermalNoise(dataFiles['RINfiles'], coatAbs)

    if dataFiles['pdhLTI'] is not None:
        ff = nosbud.freq

        # Start of data loading and calculation code.
        import json
        import scipy.signal
        # NPRO free-running frequency noise ASD:
        #                Willke et al., Opt. Lett. vol 25 no 14
        # PDH OLTFs: ctn:1504
        nproFreeASD = 10**4/ff # Hz/Hz**0.5
        print('Using {fn1}\n  and {fn2}\n      for Residual NPRO '
              'Noise Calculation.'.format(fn1=dataFiles['pdhLTI'][0],
                                          fn2=dataFiles['pdhLTI'][1]))
        # Load vector-fitted versions of PDH OLTF data
        with open(dataFiles['pdhLTI'][0], 'r') as fnorth:
            pdhNorthDict = json.load(fnorth)
        with open(dataFiles['pdhLTI'][1], 'r') as fsouth:
            pdhSouthDict = json.load(fsouth)
        pdhNorthZpk = ((np.array(pdhNorthDict['zeros.real'])
                        +1j*np.array(pdhNorthDict['zeros.imag'])),
                       (np.array(pdhNorthDict['poles.real'])
                        +1j*np.array(pdhNorthDict['poles.imag'])),
                       (pdhNorthDict['gain.real']
                        +1j*pdhNorthDict['gain.imag']))
        pdhSouthZpk = ((np.array(pdhSouthDict['zeros.real'])
                        +1j*np.array(pdhSouthDict['zeros.imag'])),
                       (np.array(pdhSouthDict['poles.real'])
                        +1j*np.array(pdhSouthDict['poles.imag'])),
                       (pdhSouthDict['gain.real']
                        +1j*pdhSouthDict['gain.imag']))
        pdhNorthLTI = scipy.signal.lti(*pdhNorthZpk)
        pdhSouthLTI = scipy.signal.lti(*pdhSouthZpk)
        _, pdhNorthMag, pdhNorthPha = pdhNorthLTI.bode(w=2*np.pi*ff)
        _, pdhSouthMag, pdhSouthPha = pdhSouthLTI.bode(w=2*np.pi*ff)
        pdhNorthMag = 10**(pdhNorthMag/20)
        pdhSouthMag = 10**(pdhSouthMag/20)
        northResidFreqPSD = (nproFreeASD / pdhNorthMag)**2
        southResidFreqPSD = (nproFreeASD / pdhSouthMag)**2
        residFreqPSD = northResidFreqPSD + southResidFreqPSD
        # End of data loading and calculation code

        nosbud.PSDList['resNPRO'] = [residFreqPSD , ff,
                                         'Residual NPRO noise'];

    print('All calculations finished.')
    print('Saving data...')
    # Save all PSD data
    nosbud.savePSD(saveList='all', filename = args.filename);

def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='This script runs noiseBudget calculations for a given '
                    'noise budget configuration file, coating structure file '
                    'and other required files.')
    group = parser.add_mutually_exclusive_group()

    group.add_argument('paramFile', nargs='?',
                       help='The parameter file for the budget',
                       default=None)

    parser.add_argument('--dataFiles', help='Data files for some '
                        'noise contributions',
                         default=None)

    parser.add_argument('-f', '--filename', help='Stem of output filename'
                        'where PSD data will be saved.', default=None)
    return parser.parse_args()

if __name__ == "__main__":
    args = grabInputArgs()
    main(args)
