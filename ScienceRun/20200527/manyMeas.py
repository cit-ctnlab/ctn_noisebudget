from measTransRIN import measTransRIN
import time
import os

dataDir = time.strftime('TransRINData_%Y%m%d_%H%M%S')
os.mkdir(dataDir)
os.chdir(dataDir)
ii = 0
while(ii<100):
    try:
        print()
        print("___________________________________________")
        print("Measurement", ii)
        print("___________________________________________")
        measTransRIN(numAvg=1)
        ii = ii + 1
    except BaseException as e:
        print("Error: ", e)
        print("Retrying in 1 min")
        time.sleep(60)
