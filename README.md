This is repo for the Caltech Coatings Thermal Noise Experiment noise budgeting.

Currently maintainied by Anchal (anchal@caltech.edu)

To install of python packages into your local env use
pip install -r requirements.txt

## Current Best Measurement of CTN Lab:

![CTN_Best_Measurement_Result](/BayesianAnalysis/CTN_Best_Measurement_Result.png)

**[CTN_Best_Measurement_Result.pdf](https://git.ligo.org/cit-ctnlab/ctn_noisebudget/-/blob/master/BayesianAnalysis/CTN_Best_Measurement_Result.pdf)**

---
Latest three beatnote spectrum with noisebudget:

**[CTN_Latest_BN_Spec](https://git.ligo.org/cit-ctnlab/ctn_noisebudget/-/blob/master/Data/dailyBeatNoteSpectrum/CTN_Latest_BN_Spec.pdf)**

All daily beatnote spectrum with noisebudget:

**[CTN_Daily_BN_Spec](https://git.ligo.org/cit-ctnlab/ctn_noisebudget/-/blob/master/Data/dailyBeatNoteSpectrum/CTN_Daily_BN_Spec.pdf)**
