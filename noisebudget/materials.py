'''
A common class for substrate and coating layer materials.
'''
import numpy as np
import uncertainties as unc
from uncertainties import ufloat as uf
import yaml

# *****************************************************************************
# Example for initialization
# *****************************************************************************
fusedSilicaParams = {
    # Default Substrate
    'Name': 'Fused Silica',
    # Substrate thickness (m)
    'Thickness': uf(0.25, 0.01)*0.0254,
    # Substrate Young's modulus (Pa)
    'Young': uf(72, 1) * 1e9,
    # Substrate Poisson Ratio (dimensionless)
    'Poisson': uf(0.170, 0.005),
    # Substrate Loss Angle (Bulk) (rad)
    'Loss': uf(1, 0.1)*1e-7,
    # Substrate Loss Angle (Shear) (rad)
    'LossShear': uf(1, 0.1)*1e-7,
    # Substrate Heat Conductivity (W/(m*K))
    'Cond': uf(1.38, 0.2),
    # Substrate Heat Capacity per unit volume (J/(K*m**3))
    'HeatCap': uf(1.6, 0.1)*1e6,
    # Substrate Refractive Index (dimensionless)
    'RefInd': uf(1.45, 0.1),
    # Substrate Thermal Expansion Coefficient (K**-1)
    'CTE': uf(5.1, 0.3)*1e-7,
    # Ignoring Thermorefractive effect in substrate (K**-1)
    'CTR': uf(0, 0),
    # Relevant Photo-elastic tensor component (dimensionless)
    'PET': uf(0.269, 0.001)
}

GaAsParams = {
    # GaAs
    'Name': 'GaAs',
    # Layer Thickness (m)
    'Thickness': uf(76.44, 0.05)*1e-9,
    # Young's Modulus (Pa)
    'Young': uf(100, 20)*1e9,
    # Poisson Ratio (dimensionless)
    'Poisson': 0.311*uf(1, 0.2),
    # Bulk Loss Angle (rad)
    'Loss': uf(2.41, 0.482)*1e-5,
    # Shear Loss Angle (rad)
    'LossShear': uf(2.41, 0.482)*1e-5,
    # Heat Conductivity (W/(m*K))
    'Cond': uf(55, 3),
    # Heat Capacity per unit volume (J/(K*m**3))
    'HeatCap': uf(1.75, 0.09)*1e6,
    # Refractive Index (dimensionless)
    'RefInd': uf(3.48, 0.03),
    # Thermal Expansion Coefficient (K**-1)
    'CTE': 5.97e-6 * uf(1, 0.1),
    # Thermorefractive Coefficient (K**-1)
    'CTR': uf(366, 7) * 1e-6,
    # Relevant Photo-elastic tensor component (dimensionless)
    'PET': uf(-0.072, 0.001)
}

# *****************************************************************************
# Class Definitions
# *****************************************************************************

# *****************************************************************************


class materials:
    def __init__(self, matParams=fusedSilicaParams):
        if isinstance(matParams, dict):
            try:
                self.Name = matParams['Name']
                self.Thick = matParams['Thickness']
                self.Young = matParams['Young']
                self.Poisson = matParams['Poisson']
                self.Loss = matParams['Loss']
                self.LossShear = matParams['LossShear']
                self.Cond = matParams['Cond']
                self.HeatCap = matParams['HeatCap']
                self.RefInd = matParams['RefInd']
                self.CTE = matParams['CTE']
                self.CTR = matParams['CTR']
                self.PET = matParams['PET']
            except BaseException:
                raise RuntimeError('Parameter dict not complete.')
        elif isinstance(matParams, str):
            try:
                with open(matParams, 'r') as mP:
                    temp = yaml.full_load(mP)
            except BaseException:
                raise RuntimeError('{fn} not found!'.format(fn=matParams))
            requiredKeys = ['Name', 'Thickness', 'Young', 'Poisson', 'Loss',
                            'LossShear', 'Cond', 'HeatCap', 'RefInd', 'CTE',
                            'CTR', 'PET']
            try:
                for key in requiredKeys:
                    if key != 'Name':
                        temp[key] = extractValue(temp[key])
            except BaseException:
                raise RuntimeError('File {fn} does not have all '
                                   'parameters.'.format(fn=matParams))
            self.Name = temp['Name']
            self.Thick = temp['Thickness']
            self.Young = temp['Young']
            self.Poisson = temp['Poisson']
            self.Loss = temp['Loss']
            self.LossShear = temp['LossShear']
            self.Cond = temp['Cond']
            self.HeatCap = temp['HeatCap']
            self.RefInd = temp['RefInd']
            self.CTE = temp['CTE']
            self.CTR = temp['CTR']
            self.PET = temp['PET']
            del temp

    @property
    def params(self):
        paramList = [
                     'Material: '+self.Name,
                     'Physical Thickness = ' + str(self.Thick) + ' m',
                     'Youngs Modulus = ' + str(self.Young) + ' Pa',
                     'Poisson Ratio = ' + str(self.Poisson),
                     'Loss Angle (Bulk) = ' + str(self.Loss) + ' rad',
                     'Loss Angle (Shear) = ' + str(self.LossShear) + ' rad',
                     'Heat Conductivity = ' + str(self.Cond) + ' W/(m*K)',
                     ('Heat Capacity per volume = ' + str(self.HeatCap)
                      + ' J/(K*m**3)'),
                     'Refractive Index = ' + str(self.RefInd),
                     ('Thermal Expansion Coefficient = ' + str(self.CTE)
                      + ' 1/K'),
                     ('Thermorefractive Coefficient = ' + str(self.CTR)
                      + ' 1/K'),
                     ('Photo-elastic tensor relevant component = '
                      + str(self.PET))
        ]
        return '\n'.join(paramList)

    def showParams(self):
        print(self.params)

    def __repr__(self):
        return self.params

    def writeParams(self, filename=None):
        '''
        This function writes a yml file for the material to be loaded later.
        Note, that yaml.dump is not used here to keep the ordering of
        parameters consistent and to write descriptions as well with comments.
        '''
        if filename is None:
            filename = self.Name.replace('+/-', 'pm')+'.yml'
        if filename.find('.yml') == -1 and filename.find('.yaml') == -1:
            filename = filename + '.yml'
        with open(filename, 'w') as f:
            f.write(fmtString(['Name: ', 11, self.Name, 34, '\n']))
            f.write(fmtString(['Thickness:', 11, str(self.Thick), 34,
                               '# m\n']))
            f.write(fmtString(['Young:', 11, str(self.Young), 34,
                               '# Pa, Youngs modulus\n']))
            f.write(fmtString(['Poisson:', 11, str(self.Poisson), 34,
                               '#dimensionless, Poisson Ratio\n']))
            f.write(fmtString(['Loss:', 11, str(self.Loss), 34,
                               '# rad, Loss Angle (Bulk)\n']))
            f.write(fmtString(['LossShear:', 11, str(self.LossShear), 34,
                               '# rad, Loss Angle (Shear)\n']))
            f.write(fmtString(['Cond:', 11, str(self.Cond), 34,
                               '# W/(m*K), Heat Conductivity\n']))
            f.write(fmtString(['HeatCap:', 11, str(self.HeatCap), 34,
                               '# J/(K*m**3), Heat Capacity per unit '
                               'volume\n']))
            f.write(fmtString(['RefInd:', 11, str(self.RefInd), 34,
                               ' # dimensionless, Refractive Index\n']))
            f.write(fmtString(['CTE:', 11, str(self.CTE), 34,
                               '# K**-1, Thermal Expansion Coefficient\n']))
            f.write(fmtString(['CTR:', 11, str(self.CTR), 34,
                               '# K**-1, Thermo-refractive Coefficient\n']))
            f.write(fmtString(['PET:', 11, str(self.PET), 34,
                               '# dimensionless, relevant photoelastic'
                               'tensor component\n']))

    def showCode(self, funcName):
        """
        This function prints out the code of the funcName function argument.
        Note: This function doesn't work for itself.
        """
        with open("materials.py", 'r') as nbm:
            allCode = nbm.readlines()
            onFunction = False
            for line in allCode:
                if onFunction:
                    if 'def ' in line:
                        onFunction = False
                    elif 'class ' in line:
                        onFunction = False
                    else:
                        print(line.replace('\n', ''))
                if 'def '+funcName in line:
                    onFunction = True
                    print(line.replace('\n', ''))

# *****************************************************************************
# *****************************************************************************


class mixedMaterials(materials):
    def __init__(self, Name, Thickness, mat1Params=GaAsParams,
                 mat2Params=GaAsParams, frac=0.5, overrideParams={}):
        for key in overrideParams.keys():
            mat1Params[key] = overrideParams[key]
            mat2Params[key] = overrideParams[key]
        mat1 = materials(mat1Params)
        mat2 = materials(mat2Params)
        self.Name = Name
        self.Thick = Thickness
        self.Young = frac*mat1.Young + (1-frac)*mat2.Young
        self.Poisson = frac*mat1.Poisson + (1-frac)*mat2.Poisson
        self.Loss = frac*mat1.Loss + (1-frac)*mat2.Loss
        self.LossShear = frac*mat1.LossShear + (1-frac)*mat2.LossShear
        self.Cond = frac*mat1.Cond + (1-frac)*mat2.Cond
        self.HeatCap = frac*mat1.HeatCap + (1-frac)*mat2.HeatCap
        self.RefInd = frac*mat1.RefInd + (1-frac)*mat2.RefInd
        self.CTE = frac*mat1.CTE + (1-frac)*mat2.CTE
        self.CTR = frac*mat1.CTR + (1-frac)*mat2.CTR
        self.PTE = frac*mat1.PTE + (1-frac)*mat2.PTE
        del mat1, mat2

# *****************************************************************************
# Helpful functions
# *****************************************************************************


def AlGaAsMix(alFrac=uf(0.92, 0.006), thickness=None):
    '''
    This function creates a material object representing Al_x Ga_1-x As
    where x is Aluminium fraction.
    '''
    GaAs = materials(matParams=GaAsParams)
    AlGaAsMixParams = {
        'Name': ('Al$_{'+str(np.round((alFrac.nominal_value), 2))
                 + '}$Ga$_{'+str(np.round(1-alFrac.nominal_value, 2))+'}$As'),
        'Thickness': 1,
        'Young': GaAs.Young,
        'Poisson': (0.311 + 0.014*alFrac)*uf(1, 0.2),
        'Loss': GaAs.Loss,
        'LossShear': GaAs.LossShear,
        # From Tara's Thesis C.1.
        'Cond': uf(55, 3)-uf(212, 3)*alFrac+uf(248, 3)*alFrac**2,
        'HeatCap': (0.33 + 0.12*alFrac) * (5.317 - 1.588*alFrac) * 1e6,
        'RefInd': GaAs.RefInd*(1-alFrac) + 2.902*alFrac,
        'CTE': (5.73 - 0.53*alFrac) * 1e-6 * uf(1, 0.1),
        'CTR': GaAs.CTR*(1-alFrac) + 163e-6*alFrac,  # From Tara's Thesis C.1
        'PET': GaAs.PET  # No idea about this
    }
    if thickness is None:
        # print('Assuming quarter wavelength thickness in AlGaAs.')
        AlGaAsMixParams['Thickness'] = 1064e-9/(4*AlGaAsMixParams['RefInd'])
    elif isinstance(thickness, unc.core.Variable):
        AlGaAsMixParams['Thickness'] = thickness
    else:
        raise RuntimeError('While providing thickness, it must be provided '
                           'using uf(nominal_value,std_dev)')
    return materials(AlGaAsMixParams)
# *****************************************************************************


def extractValue(valueStr):
    '''
    This function is to parse values loaded by yaml.full_load()
    If the value is loaded as float, it will be converted to ufloat with zero
    uncertainty.
    If the value is loaded as string with +/- operator, it will be converted
    to ufloat with the defined uncertainty.
    Valid inputs:
    1064.0
    '0.35+/-0.05'
    '(1.50+/-0.10)e+04'
    White spaces can be introduced anywhere. They will be ignored.
    '''
    if isinstance(valueStr, float):
        # Case for a float input
        return uf(valueStr, 0)
    elif valueStr.find('+/-') != -1:
        valsplit = valueStr.split('+/-')
        if '(' in valsplit[0]:
            valsplit[0] = valsplit[0].replace('(', '')
            expsplit = valsplit[1].split(')')
            valsplit[1] = expsplit[0]+expsplit[1]
            valsplit[0] = valsplit[0]+expsplit[1]
        value = float(valsplit[0].replace(' ', ''))
        uncer = float(valsplit[1].replace(' ', ''))
    else:
        # Case when uncertainity is not provided
        value = float(valueStr)
        uncer = 0.0
    return uf(value, uncer)
# *****************************************************************************


def fmtString(strList):
    '''
    This function joins a list of strings with adding white spaces in between
    the strings to ensure each string start at a particular position.
    Example:
    fmtString(['Cond:',11,str(self.Cond),32,'# W/(m*K), Heat Conductivity\n'])
    will output
    Cond:      55.0+/-3.0           # W/(m*K), Heat Conductivity
               ^11                  ^32
    '''
    returnString = ''
    for ele in strList:
        if isinstance(ele, str):
            returnString = returnString + ele
        elif isinstance(ele, int):
            padString = '{:<' + str(ele) + '}'
            returnString = padString.format(returnString)
        else:
            raise RuntimeError('List elements should be strings and integers'
                               ' only.')
    return returnString
# *****************************************************************************
