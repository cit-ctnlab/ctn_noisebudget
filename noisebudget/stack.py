'''
A class that holds coating stack structure and important parameters for
noise calculation.
'''
from materials import materials
import numpy as np
from uncertainties import ufloat as uf
from uncertainties import unumpy as unp
from ucomplex import ucomplex
import pandas as pd
import matplotlib.pyplot as plt                 # For plotting
# *****************************************************************************
# Setting RC Parameters for figure size and fontsizes
import matplotlib.pylab as pylab
params = {'legend.fontsize': 'xx-large',
          'figure.figsize': (16, 12),
          'axes.labelsize': 'xx-large',
          'axes.titlesize': 'xx-large',
          'xtick.labelsize': 'xx-large',
          'ytick.labelsize': 'xx-large'}
pylab.rcParams.update(params)
# *****************************************************************************


# *****************************************************************************
class stack:
    def __init__(self, structure=None, evenLayer=None, oddLayer=None,
                 substrate=None, nol=2, qwl=False, lam=1064e-9,
                 lightInit=False):
        # Not specifying structure, but even, odd and substrate layers
        # is sufficient
        if structure is None:
            tempmatName = [None]*nol
            self.PhyThick = np.ones(nol)*uf(1, 1)
            self.OptThick = np.ones(nol)*uf(1, 1)
            self.Young = np.ones(nol)*uf(1, 1)
            self.Poisson = np.ones(nol)*uf(1, 1)
            self.Loss = np.ones(nol)*uf(1, 1)
            self.LossShear = np.ones(nol)*uf(1, 1)
            self.Cond = np.ones(nol)*uf(1, 1)
            self.HeatCap = np.ones(nol)*uf(1, 1)
            self.RefInd = np.ones(nol)*uf(1, 1)
            self.CTE = np.ones(nol)*uf(1, 1)
            self.CTR = np.ones(nol)*uf(1, 1)
            self.PET = np.ones(nol)*uf(1, 1)
            try:
                if qwl:
                    if evenLayer.RefInd < oddLayer.RefInd:
                        self.PhyThick[0] = lam/(2*evenLayer.RefInd)
                    else:
                        self.PhyThick[0] = lam/(4*evenLayer.RefInd)
                    self.PhyThick[2::2] = lam/(4*evenLayer.RefInd)
                else:
                    self.PhyThick[0::2] = evenLayer.Thick
                for ii in range(0, nol, 2):
                    tempmatName[ii] = [evenLayer.Name]
                self.Young[0::2] = evenLayer.Young
                self.Poisson[0::2] = evenLayer.Poisson
                self.Loss[0::2] = evenLayer.Loss
                self.LossShear[0::2] = evenLayer.LossShear
                self.Cond[0::2] = evenLayer.Cond
                self.HeatCap[0::2] = evenLayer.HeatCap
                self.RefInd[0::2] = evenLayer.RefInd
                self.CTE[0::2] = evenLayer.CTE
                self.CTR[0::2] = evenLayer.CTR
                self.PET[0::2] = evenLayer.PET
            except BaseException:
                raise RuntimeError('Even Coating Layer Object is incomplete.')
            try:
                if qwl:
                    self.PhyThick[1::2] = lam/(4*oddLayer.RefInd)
                else:
                    self.PhyThick[1::2] = [oddLayer.Thick]
                for ii in range(1, nol, 2):
                    tempmatName[ii] = [oddLayer.Name]
                self.Young[1::2] = oddLayer.Young
                self.Poisson[1::2] = oddLayer.Poisson
                self.Loss[1::2] = oddLayer.Loss
                self.LossShear[1::2] = oddLayer.LossShear
                self.Cond[1::2] = oddLayer.Cond
                self.HeatCap[1::2] = oddLayer.HeatCap
                self.RefInd[1::2] = oddLayer.RefInd
                self.CTE[1::2] = oddLayer.CTE
                self.CTR[1::2] = oddLayer.CTR
                self.PET[1::2] = oddLayer.PET
            except BaseException:
                raise RuntimeError('Odd Coating Layer Object is incomplete.')
            self.matName = [x[0] for x in tempmatName]
            try:
                if qwl:
                    if self.RefInd[-1] > self.RefInd[-2]:
                        if substrate.RefInd > self.RefInd[-1]:
                            self.PhyThick[-1] = lam/(2*self.RefInd[-1])
                    elif substrate.RefInd < self.RefInd[-1]:
                        self.PhyThick[-1] = lam/(2*self.RefInd[-1])
                self.matName += [substrate.Name]
                self.PhyThick = np.append(self.PhyThick, substrate.Thick)
                self.Young = np.append(self.Young, substrate.Young)
                self.Poisson = np.append(self.Poisson, substrate.Poisson)
                self.Loss = np.append(self.Loss, substrate.Loss)
                self.LossShear = np.append(self.LossShear, substrate.LossShear)
                self.Cond = np.append(self.Cond, substrate.Cond)
                self.HeatCap = np.append(self.HeatCap, substrate.HeatCap)
                self.RefInd = np.append(self.RefInd, substrate.RefInd)
                self.CTE = np.append(self.CTE, substrate.CTE)
                self.CTR = np.append(self.CTR, substrate.CTR)
                self.PET = np.append(self.PET, substrate.PET)
            except BaseException:
                raise RuntimeError('Substrate Object is incomplete.')
        # Structure can be a dictionary of arrays
        elif isinstance(structure, dict):
            self.matName = structure['matName']
            self.PhyThick = structure['PhyThick']
            self.Young = structure['Young']
            self.Poisson = structure['Poisson']
            self.Loss = structure['Loss']
            self.LossShear = structure['LossShear']
            self.Cond = structure['Cond']
            self.HeatCap = structure['HeatCap']
            self.RefInd = structure['RefInd']
            self.CTE = structure['CTE']
            self.CTR = structure['CTR']
            self.PET = structure['PET']
        # Structure can be a csv filename with 20 columns as follow:
        elif isinstance(structure, str):
            try:
                # coatStackTable = np.loadtxt(structure, delimiter=',',
                #                        usecols=(3,), unpack=1, comments='%')
                tempdf = pd.read_csv(structure, sep=',', header=0)
            except BaseException:
                raise RuntimeError('Error in reading '
                                   '{fn}'.format(fn=structure))
            try:
                self.matName = list(tempdf['MaterialName'])
                self.PhyThick = unp.uarray(tempdf['PhysicalThicknessNV'],
                                           tempdf['PhysicalThicknessSTD'])
                self.Young = unp.uarray(tempdf['YoungModulusNV'],
                                        tempdf['YoungModulusSTD'])
                self.Poisson = unp.uarray(tempdf['PoissonRatioNV'],
                                          tempdf['PoissonRatioSTD'])
                self.Loss = unp.uarray(tempdf['LossAngleNV'],
                                       tempdf['LossAngleSTD'])
                self.LossShear = unp.uarray(tempdf['LossAngleShearNV'],
                                            tempdf['LossAngleShearSTD'])
                self.Cond = unp.uarray(tempdf['HeatConductivityNV'],
                                       tempdf['HeatConductivitySTD'])
                self.HeatCap = unp.uarray(tempdf['HeatCapacityNV'],
                                          tempdf['HeatCapacitySTD'])
                self.RefInd = unp.uarray(tempdf['RefractiveIndexNV'],
                                         tempdf['RefractiveIndexSTD'])
                self.CTE = unp.uarray(tempdf['CTENV'], tempdf['CTESTD'])
                self.CTR = unp.uarray(tempdf['CTRNV'], tempdf['CTRSTD'])
                self.PET = unp.uarray(tempdf['PETNV'], tempdf['PETSTD'])
            except BaseException:
                raise RuntimeError('Coating Stack file {fn} '
                                   'is incomplete.'.format(fn=structure))
        # Calculating derived quantities
        if qwl:
            self.OptThick = np.ones(nol+1)*uf(1, 0)*lam/4
            self.OptThick[-1] = self.PhyThick[-1]*self.RefInd[-1]
        else:
            self.OptThick = self.PhyThick*self.RefInd
        # Wavelength of light in jth layer
        self.WaveLength = lam/self.RefInd
        # Coefficient of photoelastic effect
        # PRD 87, 082001 Eq (A5)
        self.CPE = -0.5*self.PET*self.RefInd**3

        # Initialize effect beam area on each layer
        self.Aeff = np.ones(nol+1)*uf(0, 0)

        # Effective coating and substrate paramters
        self.coatPhyThick = np.sum(self.PhyThick[0:-1])
        self.coatHeatCap = np.sum(self.HeatCap[0:-1]
                                  * self.PhyThick[0:-1] / self.coatPhyThick)
        self.coatCond = self.coatPhyThick / np.sum(self.PhyThick[:-1]
                                                   / self.Cond[:-1])
        if not lightInit:
            self.HongBrownianCoeffCalculations(lam)
            self.EvanThermoOpticCoeffCalculations(lam)
        else:
            print('Run stack.HongBrownianCoeffCalculations(lam) and '
                  'stack.EvanThermoOpticCoeffCalculations(lam) to calculate '
                  'the coefficients required for coating brownian noise and '
                  'coating thermo-optic noise calculations.')

    def HongBrownianCoeffCalculations(self, lam):
        '''
        This function calculates important parameters for coating Brownian
        noise calculation.
        This follows from Hong et al . PRD 87, 082001 (2013)
        '''
        print('Using Hong et al . PRD 87, 082001 (2013) to calculate '
              'parameters for coating brownian noise')
        print("Wait 1 to 2 minutes ...")
        # Number of coating layers
        nol = len(self.PhyThick)-1
        # Local substrate variables for ease in coding.
        subPoisson = self.Poisson[-1]   # Last layer is substrate
        subYoung = self.Young[-1]

        # Reflectivities and transmitivities
        # Refl[j] is reflectivity from (j-1)th and (j)th layer interface
        # Here Refl[0] is reflectivity from air and 0th layer
        # and Refl[-1] is reflectivity between last layer and substrate
        self.Refl = np.ones(nol+1)*uf(1, 1)
        self.Refl[0] = (1 - self.RefInd[0])/(1 + self.RefInd[0])
        self.Refl[1:] = ((self.RefInd[0:-1] - self.RefInd[1:])
                         / (self.RefInd[0:-1] + self.RefInd[1:]))
        self.Tran = usqrt(1 - self.Refl*self.Refl)
        # Phase shift in propagation once (Not roundtrip)
        self.Phi = self.OptThick*2*np.pi / lam

        # Reflection and transmission matrices
        # Rmat[j] is reflection matrix for interface of (j-1)th and (j)th layer
        # Tmat[j] is transmission through (j)th layer
        # Note: Tmat[-1] is transmission through substrate and is actually not
        #       used in below calculations
        self.Rmat = []
        self.Tmat = []
        for ii in range(nol+1):
            self.Rmat += [reflectionMatrix(self.Refl[ii], self.Tran[ii])]
            self.Tmat += [phaseShiftMatrix(self.Phi[ii])]

        # Effective reflectivity of each layer and light amplitude there
        self.rEff = np.ones(nol+1)*ucomplex(x=1, y=0)
        lastTF = self.Rmat[-1]
        self.LightAmp = []
        self.LightAmp.insert(0, np.matrix([[ucomplex(1)], [ucomplex(0)]]))
        self.rEff[-1] = self.Refl[-1]

        for ii in range(nol-1, -1, -1):
            TF = lastTF*self.Tmat[ii]*self.Rmat[ii]
            self.rEff[ii] = -TF[1, 0]/TF[1, 1]
            lastTF = TF
            self.LightAmp.insert(0, [TF*self.LightAmp[-1]])

        self.rho = self.rEff[0]          # Complex reflectivity of Coating
        # Next we calculate derivatives of rho with respect to phi_k and r_j
        self.delRho_delPhik, self.delRho_delReflk = diffwrtPhiandRefl(
                                                                self.Rmat,
                                                                self.Tmat,
                                                                self.Refl,
                                                                self.Phi)
        self.delLogRho_delPhik = self.delRho_delPhik / self.rho
        self.delLogRho_delReflk = self.delRho_delReflk / self.rho
        self.delRho_delPhik[-1] = ucomplex(0)      # Define this as per Eq (26)

        # Define the function epsilon as per Eq (25)
        def epsilon(j, z):
            '''
            This function is returning nominal_value as std_dev calculation
            becomes very lengthy because of its presense.
            Somehow, calculating derivatives becomes harder with epsilon.
            This still needs to be probed more.
            '''
            # zjp1 is z coordinate of top of (j+1)th layer.
            # z=0 at substrate top surface.
            if j == nol-1:
                zjp1 = 0
            else:
                zjp1 = np.sum(self.PhyThick[j+1:nol])
            k0 = 2*np.pi/lam
            A = (self.RefInd[j] + self.CPE[j])*self.delLogRho_delPhik[j]
            B1 = self.CPE[j]*((self.delLogRho_delPhik[j]*(1 - self.Refl[j]**2)
                               / (2*self.Refl[j]))
                              - (self.delLogRho_delPhik[j+1]
                                 * (1 + self.Refl[j]**2)/(2*self.Refl[j])))
            B2 = unp.cos(2*k0*self.RefInd[j]*(z-zjp1))
            C1 = (1 - self.Refl[j]**2)*self.CPE[j]*self.delLogRho_delReflk[j]
            C2 = unp.sin(2*k0*self.RefInd[j]*(z-zjp1))
            # return A - B1*B2 - C1*C2
            return (A - B1*B2 - C1*C2).nominal_value
        self.epsilon = epsilon

        # Define transfer functions from bulk and shear noise fields to layer
        # thickness and surface height as pwe Table I in paper
        self.C_B = unp.sqrt(0.5*(1+self.Poisson))
        self.C_SA = unp.sqrt(1 - 2*self.Poisson)
        self.D_B = ((1 - subPoisson - 2*subPoisson**2)*self.Young
                    / (unp.sqrt(2*(1+self.Poisson))*subYoung))
        self.D_SA = -((1 - subPoisson - 2*subPoisson**2)*self.Young
                      / (2*unp.sqrt(1-2*self.Poisson)*subYoung))
        self.D_SB = (np.sqrt(3)*(1-self.Poisson)
                     * (1 - subPoisson - 2*subPoisson**2)*self.Young
                     / (2*unp.sqrt(1-2*self.Poisson)
                        * (1+self.Poisson)*subYoung))

    def EvanThermoOpticCoeffCalculations(self, lam):
        '''
        This function calculates detCTE and coatCTR, two final coefficients
        in the calculation of coating thermo-optic noise.
        This follows from Evans et al . PRD 78, 102003 (2008)
        '''
        print('Using Evans et al . PRD 78, 102003 (2008) to calculate'
              'coefficients for thermo-optic noise')
        print('Wait 30 to 90 seconds ...')
        # Following is from Evans et al. PRD 78, 102003 (2008)
        # for ThermoOptic noise
        nol = len(self.Refl)-1          # Number of coating layers
        # Local substrate variables for ease in coding.
        subPoisson = self.Poisson[-1]   # Last layer is substrate
        subYoung = self.Young[-1]
        subHeatCap = self.HeatCap[-1]

        # Effective Thermo Elastic coefficients (A1)
        self.CTEeff = (self.CTE * (1+subPoisson)/(1-self.Poisson)
                       * ((1+self.Poisson)/(1+subPoisson)
                          + (1 - 2*subPoisson)*self.Young/subYoung))
        self.CTEyama = self.CTE * (1+self.Poisson) / (1-self.Poisson)

        # Effective coating and substrate Thermo Elastic coefficients (A2)
        self.coatCTE = np.sum(self.CTEeff[0:-1]*self.PhyThick[0:-1]
                              / self.coatPhyThick)
        self.coatCTEyama = np.sum(self.CTEyama[0:-1]*self.PhyThick[0:-1]
                                  / self.coatPhyThick)
        # This is same as self.CTEeff[-1]
        self.subCTE = 2*self.CTE[-1]*(1+subPoisson)

        # Evans' formula for effective reflectivity (B3)
        # We first define Ephi locally.
        # Ephi[k] is roundtrip phase for light in (k-1)th layer
        # So, Ephi[0] is for air which is kept zero.
        # Note: Factor of 2 is there before Phi as in Evan's paper,
        #       Phi is round trip phase while in our class it is one way
        #       propagation phase (as used in Hong et al's paper.)
        Ephi = np.zeros(nol+1)*uf(0, 0)
        Ephi[1:] = -2*self.Phi[0:-1]
        self.ErEff = np.ones(nol+1)*ucomplex(x=0, y=0)
        # Eq B4.
        # This is effective reflectivity between substrate and last layer
        self.ErEff[nol] = ucomplex(mag=1, ang=-Ephi[nol])*self.Refl[-1]
        # Recursion starts from there
        for ii in range(nol-1, -1, -1):
            self.ErEff[ii] = (ucomplex(mag=1, ang=-Ephi[ii])
                              * (self.ErEff[ii+1]+self.Refl[ii])
                              / (self.ErEff[ii+1]*self.Refl[ii]+1))

        # Evan's formula for delEvanrEff_k/delPhi_j (B5)
        self.delErEff_delPhik = np.ones([nol+1, nol+1])*uf(1, 1)
        for k in range(nol, -1, -1):
            for j in range(nol+1):
                if k < j:
                    r_k = self.Refl[k]
                    rb_kp1 = self.ErEff[k+1]
                    expPhi_k = ucomplex(mag=1, ang=-Ephi[k])
                    self.delErEff_delPhik[k, j] = (expPhi_k*(1 - r_k**2)
                                                   * self.delErEff_delPhik[k+1,
                                                                           j]
                                                   / ((r_k*rb_kp1 + 1)**2))
                elif k == j:
                    self.delErEff_delPhik[k, j] = (ucomplex(x=0, y=-1)
                                                   * self.ErEff[k])
                else:
                    self.delErEff_delPhik[k, j] = 0

        # Evan's formula for delPhic_delPhik (B6)
        self.delPhic_delPhik = np.ones([nol+1])*uf(1, 1)
        for k in range(nol+1):
            self.delPhic_delPhik[k] = (self.delErEff_delPhik[0, k]
                                       / self.ErEff[0]).imag

        # Evan's formula for delPhi_k/delT (B8)
        # Note, we are ignoring last element as it is for substrate parameters
        #      and not involved in calculations ahead.
        self.delPhik_delT = (4*np.pi*(self.CTR + self.CTEeff*self.RefInd)
                             * self.PhyThick/lam)[:-1]
        self.delPhi0_delT = -4*np.pi*np.sum(self.CTEeff*self.PhyThick)/lam

        # Evan's formula for delPhic/delT
        self.delPhic_delT = (np.sum(self.delPhic_delPhik[1:]*self.delPhik_delT)
                             + self.delPhic_delPhik[0]*self.delPhi0_delT)

        # Some of the above calculations are just for completeness
        # For actualy getting delCTE and coatCTR, we do not need the above
        # calculated delPhic_delT
        '''
        This is an attempt to remove above calculation and simply use
        calculation done in HongBrownianCoeffCalculations()
        self.delPhik_delT = (4*np.pi*(self.CTR + self.CTEeff*self.RefInd)
                             * self.PhyThick/lam)[:-1]
        self.delPhic_delPhik = np.ones([nol])*uf(1, 0)
        for ii in range(nol):
            self.delPhic_delPhik[ii] = 0.5*self.delLogRho_delPhik[ii].imag

        # Evan's formula for coatCTR (beta bar) (derived from B7,8,13 and 21)
        self.coatCTR = -np.sum(self.delPhic_delPhik
                               * self.delPhik_delT) / (4*np.pi)
        '''

        # Evan's formula for delCTE (Delta alpha bar) (Eq 18)
        self.delCTE = self.coatCTE - (self.subCTE*self.coatHeatCap/subHeatCap)

        # Evan's formula for coatCTR (beta bar) (derived from B7,8,13 and 21)
        self.coatCTR = -np.sum(self.delPhic_delPhik[1:]
                               * self.delPhik_delT) / (4*np.pi)

    def getSub(self):
        subDict = {
            'Name': self.matName[-1],
            'Thickness': self.PhyThick[-1],
            'Young': self.Young[-1],
            'Poisson': self.Poisson[-1],
            'Loss': self.Loss[-1],
            'LossShear': self.LossShear[-1],
            'Cond': self.Cond[-1],
            'HeatCap': self.HeatCap[-1],
            'RefInd': self.RefInd[-1],
            'CTE': self.CTE[-1],
            'CTR': self.CTR[-1],
            'PET': self.PET[-1],
        }
        return materials(matParams=subDict)

    def writeParams(self, filename):
        nol = len(self.PhyThick)
        coatStackTable = np.zeros([nol, 22])
        for ii in range(nol):
            coatStackTable[ii, 0] = self.PhyThick[ii].nominal_value
            coatStackTable[ii, 1] = self.PhyThick[ii].std_dev
            coatStackTable[ii, 2] = self.Young[ii].nominal_value
            coatStackTable[ii, 3] = self.Young[ii].std_dev
            coatStackTable[ii, 4] = self.Poisson[ii].nominal_value
            coatStackTable[ii, 5] = self.Poisson[ii].std_dev
            coatStackTable[ii, 6] = self.Loss[ii].nominal_value
            coatStackTable[ii, 7] = self.Loss[ii].std_dev
            coatStackTable[ii, 8] = self.LossShear[ii].nominal_value
            coatStackTable[ii, 9] = self.LossShear[ii].std_dev
            coatStackTable[ii, 10] = self.Cond[ii].nominal_value
            coatStackTable[ii, 11] = self.Cond[ii].std_dev
            coatStackTable[ii, 12] = self.HeatCap[ii].nominal_value
            coatStackTable[ii, 13] = self.HeatCap[ii].std_dev
            coatStackTable[ii, 14] = self.RefInd[ii].nominal_value
            coatStackTable[ii, 15] = self.RefInd[ii].std_dev
            coatStackTable[ii, 16] = self.CTE[ii].nominal_value
            coatStackTable[ii, 17] = self.CTE[ii].std_dev
            coatStackTable[ii, 18] = self.CTR[ii].nominal_value
            coatStackTable[ii, 19] = self.CTR[ii].std_dev
            coatStackTable[ii, 20] = self.PET[ii].nominal_value
            coatStackTable[ii, 21] = self.PET[ii].std_dev
        tempdf = pd.DataFrame(coatStackTable,
                              columns=['PhysicalThicknessNV',
                                       'PhysicalThicknessSTD',
                                       'YoungModulusNV',
                                       'YoungModulusSTD', 'PoissonRatioNV',
                                       'PoissonRatioSTD', 'LossAngleNV',
                                       'LossAngleSTD', 'LossAngleShearNV',
                                       'LossAngleShearSTD',
                                       'HeatConductivityNV',
                                       'HeatConductivitySTD', 'HeatCapacityNV',
                                       'HeatCapacitySTD', 'RefractiveIndexNV',
                                       'RefractiveIndexSTD', 'CTENV', 'CTESTD',
                                       'CTRNV', 'CTRSTD', 'PETNV', 'PETSTD'])
        tempse = pd.Series(self.matName)
        tempdf['MaterialName'] = tempse.values
        tempdf.to_csv(filename, sep=',', index=False)

    def plotLightAmpProfile(self):
        nol = len(self.LightAmp)
        thelen = np.arange(0, nol)
        FwdLightInt = np.ones(nol)*uf(1, 0)
        BwdLightInt = np.zeros(nol)*uf(1, 0)
        for ii in range(nol-1):
            FwdAmp = self.LightAmp[ii][0][0, 0]
            BwdAmp = self.LightAmp[ii][0][1, 0]
            FwdLightInt[ii] = FwdAmp.abs()*FwdAmp.abs()
            BwdLightInt[ii] = BwdAmp.abs()*BwdAmp.abs()
        TotalLightInt = FwdLightInt + BwdLightInt
        fig = plt.figure(figsize=(16, 12))
        ax = fig.gca()
        ax.errorbar(thelen, unp.nominal_values(FwdLightInt),
                    unp.std_devs(FwdLightInt), fmt='bo')
        ax.errorbar(thelen, unp.nominal_values(BwdLightInt),
                    unp.std_devs(BwdLightInt), fmt='ro')
        ax.errorbar(thelen, unp.nominal_values(TotalLightInt),
                    unp.std_devs(TotalLightInt), fmt='ko')
        ax.legend(['Forward Light Intensity', 'Backward Light Intensity',
                   'Total Light Intensity'])
        ax.set_yscale('log')
        ax.set_xlabel('Layer number')
        ax.set_ylabel('Light Intensity relative to transmitted'
                      'light intensity')
        ax.set_title('Light Penetration Profile')
        ax.grid(True)
        return fig

    def showCode(self, funcName):
        """
        This function prints out the code of the funcName function argument.
        Note: This function doesn't work for itself.
        """
        with open("stack.py", 'r') as nbm:
            allCode = nbm.readlines()
            onFunction = False
            for line in allCode:
                if onFunction:
                    if 'def ' in line:
                        onFunction = False
                    elif 'class ' in line:
                        onFunction = False
                    else:
                        print(line.replace('\n', ''))
                if 'def '+funcName in line:
                    onFunction = True
                    print(line.replace('\n', ''))

# *****************************************************************************


# *****************************************************************************
# Helpful functions
# *****************************************************************************
def usqrt(arg):
    '''
    Square root for ufloat including option for complex answers.
    '''
    if isinstance(arg, np.ndarray):
        lis = []
        for a in arg:
            lis += [usqrt(a)]
        return np.array(lis)
    else:
        if arg.nominal_value < 0:
            return ucomplex(uf(0, 0), uf(1, 0))*unp.sqrt(-arg+uf(0, 0))
        else:
            return ucomplex(unp.sqrt(arg)+uf(0, 0))


# *****************************************************************************
def reflectionMatrix(Refl, Tran):
    a = ucomplex(1)/Tran
    b = ucomplex(Refl)/Tran
    return np.matrix([[a, -b], [-b, a]])


# *****************************************************************************
def phaseShiftMatrix(Phi):
    exp_to_iphi = ucomplex(mag=1, ang=Phi)
    exp_to_iphiconj = ucomplex(mag=1, ang=-Phi)
    return np.matrix([[exp_to_iphi, 0], [0, exp_to_iphiconj]])


# *****************************************************************************
def diffwrtPhiandRefl(Rmat, Tmat, Refl, Phi):
    '''
    This function calculates derivatives of complex reflectivity of Coating
    with respect to phase shifts through each layer and reflectivities of
    each interface
    This is done by reducing the matrix product of transfer function into a
    product of three matrices Mprev, M and Mnext where M is the Rmat or Tmat
    matrix consisting of differentiating variable and Mprev (and Mnext) is
    product of all matrices that come on left (right) of it in full transfer
    function product.
    The formulas are derived by mathematica in file:
    coatingReflectivityDerivativesExpressions.nb
    '''
    nol = len(Refl)-1                          # Number of coating layers
    delRho_delPhik = np.zeros(nol+1)*ucomplex(x=0, y=0)
    delRho_delReflk = np.zeros(nol+1)*ucomplex(x=0, y=0)
    for j in range(nol+1):
        Mp = np.matrix([[ucomplex(1), ucomplex(0)],
                        [ucomplex(0), ucomplex(1)]])
        Mn = np.matrix([[ucomplex(1), ucomplex(0)],
                        [ucomplex(0), ucomplex(1)]])
        for k in range(j):
            Mn = Tmat[k]*Rmat[k]*Mn
        for k in range(j, nol):
            Mp = Rmat[k+1]*Tmat[k]*Mp
        # Note: A negative sign was introduced to match results of Fig5.
        # from the calculations in coatingReflectivityDerivativesExpressions.nb
        delRho_delReflk[j] = ((-Mn[0, 1]*Mn[1, 0] + Mn[0, 0]*Mn[1, 1])
                              * (Mp[1, 0]**2 - Mp[1, 1]**2)
                              / (Mn[1, 1]*(-Refl[j]*Mp[1, 0] + Mp[1, 1])
                                 + Mn[0, 1]*(Mp[1, 0] - Refl[j]*Mp[1, 1]))**2)
    for j in range(nol):
        Mp = Rmat[j+1]
        Mn = Rmat[0]
        for k in range(1, j+1):
            Mn = Rmat[k]*Tmat[k-1]*Mn
        for k in range(j+2, nol+1):
            Mp = Rmat[k]*Tmat[k-1]*Mp
        exp2iPhi = ucomplex(mag=1, ang=2*Phi[j])
        delRho_delPhik[j] = (ucomplex(x=0, y=2)*exp2iPhi
                             * (Mn[0, 1]*Mn[1, 0] - Mn[0, 0]*Mn[1, 1])
                             * Mp[1, 0]*Mp[1, 1]
                             / (exp2iPhi*Mn[0, 1]*Mp[1, 0]
                                + Mn[1, 1]*Mp[1, 1])**2)

    # Note derivates wrt Refl[0], Refl[1] ... Refl[nol] is taken
    #    while wrt Phi[0], Phi[1] ... Phi[nol-1] is taken and
    #          wrt to Phi[nol] is set to 0 in initialization as in Eq 26

    return delRho_delPhik, delRho_delReflk
