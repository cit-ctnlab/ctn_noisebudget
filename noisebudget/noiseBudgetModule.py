'''
This is python module for the noise budget calculation.
'''

import time
import pickle
import os
import numpy as np
import pandas as pd
import yaml
import scipy.constants as scc
import scipy.integrate as scint
import scipy.special as scsp
import uncertainties as unc
from materials import materials, AlGaAsMix
from stack import stack
from uncertainties import ufloat as uf
from uncertainties import unumpy as unp
from uncertainties.unumpy import nominal_values as unpnv
from uncertainties.unumpy import std_devs as unpstd

import matplotlib.pyplot as plt  # For plotting
# *****************************************************************************
# Setting RC Parameters for figure size and fontsizes
import matplotlib.pylab as pylab
params = {'legend.fontsize': 'xx-large',
          'figure.figsize': (20, 10),
          'axes.labelsize': 'xx-large',
          'axes.titlesize': 'xx-large',
          'xtick.labelsize': 'xx-large',
          'ytick.labelsize': 'xx-large'}
pylab.rcParams.update(params)
# *****************************************************************************

# *****************************************************************************
# Default values and also example for initialization
# *****************************************************************************
fusedSilicaParams = {
    # Default Substrate
    'Name': 'Fused Silica',
    # Substrate thickness (m)
    'Thickness': uf(0.25, 0.01)*0.0254,
    # Substrate Young's modulus (Pa)
    'Young': uf(72, 1) * 1e9,
    # Substrate Poisson Ratio (dimensionless)
    'Poisson': uf(0.170, 0.005),
    # Substrate Loss Angle (Bulk) (rad)
    'Loss': uf(1, 0.1)*1e-7,
    # Substrate Loss Angle (Shear) (rad)
    'LossShear': uf(1, 0.1)*1e-7,
    # Substrate Heat Conductivity (W/(m*K))
    'Cond': uf(1.38, 0.2),
    # Substrate Heat Capacity per unit volume (J/(K*m**3))
    'HeatCap': uf(1.6, 0.1)*1e6,
    # Substrate Refractive Index (dimensionless)
    'RefInd': uf(1.45, 0.1),
    # Substrate Thermal Expansion Coefficient (K**-1)
    'CTE': uf(5.1, 0.3)*1e-7,
    # Ignoring Thermorefractive effect in substrate (K**-1)
    'CTR': uf(0, 0),
    # Relevant Photo-elastic tensor component (dimensionless)
    'PET': uf(0.269, 0.001)
}
fusedSilica = materials(matParams=fusedSilicaParams)

GaAsParams = {
    # GaAs
    'Name': 'GaAs',
    # Layer Thickness (m)
    'Thickness': uf(76.44, 0.05)*1e-9,
    # Young's Modulus (Pa)
    'Young': uf(100, 20)*1e9,
    # Poisson Ratio (dimensionless)
    'Poisson': 0.311*uf(1, 0.2),
    # Bulk Loss Angle (rad)
    'Loss': uf(2.41, 0.482)*1e-5,
    # Shear Loss Angle (rad)
    'LossShear': uf(2.41, 0.482)*1e-5,
    # Heat Conductivity (W/(m*K))
    'Cond': uf(55, 3),
    # Heat Capacity per unit volume (J/(K*m**3))
    'HeatCap': uf(1.75, 0.09)*1e6,
    # Refractive Index (dimensionless)
    'RefInd': uf(3.48, 0.03),
    # Thermal Expansion Coefficient (K**-1)
    'CTE': 5.97e-6 * uf(1, 0.1),
    # Thermorefractive Coefficient (K**-1)
    'CTR': uf(366, 7) * 1e-6,
    # Relevant Photo-elastic tensor component (dimensionless)
    'PET': uf(-0.072, 0.001)
}
GaAs = materials(matParams=GaAsParams)
AlGaAs = AlGaAsMix(alFrac=uf(0.92, 0.006))

defaultParams = {
    'Name': 'Default Noise Budget',  # Can put date-time here
    # Environmental
    'temp': uf(305, 1),                    # K, Temperature
    # Geometrical & Optical
    'cavLen': uf(1.45, 0.01) * 0.0254,    # m, Cavity Length
    'lam': 1064e-9,                       # m, Laser wavelength
    'pdhModInd': uf(0.2, 0.005),          # rad, PDG Modulation Index
    'cavVis': uf(0.35, 0.05),             # dimensionless, Cavity Visibility
    'powInc': uf(3, 0.2)*1e-3,            # W, Incident Power
    'finesse': uf(15000, 1000),           # dimensionless, Cavity Finesse
    'roc': uf(1000, 5) * 1e-3,       # m, Radius of curvature of Cavity Mirrors
    'mirRad': uf(0.5, 0.01) * 0.0254,      # m, Radius of mirrors
    'nom': 4,                          # No. of mirrors involved in experiment.
    'nob': 2,                             # No. of laser beams in experiment
    # Substrate
    'substrate': fusedSilica,  # Substrate material object
    # Top and even numbered Coating (Numbers starting from 0) (Default GaAs)
    'evenCL': GaAs,
    # Odd numbered Coating (Numbers starting from 0) (Default AlGaAs)
    'oddCL': AlGaAs,
    # Coating Structure (Dictionary of arrays of material properties)
    'structure': None,
    'nol': 57,  # Number of coating layers
    'freq': np.logspace(-1, 4, 300),  # Frequency points for noise budget
    'qwl': True
}

# *****************************************************************************
# Class Definition
# *****************************************************************************


class noiseBudget:
    def __init__(self, params=defaultParams, lightInit=False):
        # params can be a file name where all parameters are written
        if isinstance(params, str):
            try:
                with open(params, 'r') as p:
                    temp = yaml.full_load(p)
            except BaseException:
                raise RuntimeError('{fn} not found!'.format(fn=params))

            flagCode = 0
            for key, value in temp.items():
                if ((key == 'substrate')
                        or (key == 'evenCL')
                        or (key == 'oddCL')):
                    temp[key] = materials(value)
                    flagCode = flagCode + 1
                elif key == 'structure':
                    flagCode = 3
                elif (key == 'nol'
                        or key == 'fnop'
                        or key == 'nom'
                        or key == 'nob'):
                    temp[key] = int(value)
                elif key == 'fhigh' or key == 'flow' or key == 'lam':
                    temp[key] = float(value)
                elif key == 'qwl':
                    temp[key] = bool(value)
                elif key != 'Name':
                    temp[key] = extractValue(value)

            defaultNoneKeys = ['substrate', 'evenCL', 'oddCL', 'structure']
            for key in defaultNoneKeys:
                if key not in list(temp.keys()):
                    temp[key] = None

            if flagCode < 3:
                raise RuntimeError(
                      'File {fn} needs to have either coatStack parameter or'
                      ' set of substrate, evenCL and oddCL.'.format(fn=params))

            # Check if all requried info has been provided
            requiredKeys = ['Name', 'temp', 'cavLen', 'lam', 'pdhModInd',
                            'cavVis', 'powInc', 'finesse', 'roc', 'mirRad',
                            'nom', 'nob', 'nol', 'flow', 'fhigh', 'fnop',
                            'qwl', 'structure', 'substrate', 'evenCL', 'oddCL']
            for key in requiredKeys:
                if key not in temp:
                    print(key, ' not present.')
                    raise RuntimeError('File  {fn} does not have all '
                                       'parameters.'.format(fn=params))

            # Everything looks good, so continue
            self.Name = temp['Name']
            self.temp = temp['temp']
            self.cavLen = temp['cavLen']
            self.lam = temp['lam']
            self.pdhModInd = temp['pdhModInd']
            self.cavVis = temp['cavVis']
            self.powInc = temp['powInc']
            self.finesse = temp['finesse']
            self.roc = temp['roc']
            self.mirRad = temp['mirRad']
            self.nom = temp['nom']
            self.nob = temp['nob']
            self.sub = temp['substrate']
            self.evenCL = temp['evenCL']
            self.oddCL = temp['oddCL']
            self.structure = temp['structure']
            self.sub = temp['substrate']
            self.nol = temp['nol']
            self.freq = np.logspace(np.log10(temp['flow']),
                                    np.log10(temp['fhigh']), temp['fnop'])
            self.qwl = temp['qwl']
            print('Creating coating stack for the noise budget...')
            self.coatStack = stack(structure=temp['structure'],
                                   evenLayer=temp['evenCL'],
                                   oddLayer=temp['oddCL'],
                                   substrate=temp['substrate'],
                                   nol=temp['nol'],
                                   qwl=self.qwl,
                                   lam=self.lam,
                                   lightInit=lightInit)
            self.sub = self.coatStack.getSub()
        # Or params can be a dictionary containing all the parameters
        elif isinstance(params, dict):
            try:
                self.Name = params['Name']
                self.temp = params['temp']
                self.cavLen = params['cavLen']
                self.lam = params['lam']
                self.pdhModInd = params['pdhModInd']
                self.cavVis = params['cavVis']
                self.powInc = params['powInc']
                self.finesse = params['finesse']
                self.roc = params['roc']
                self.mirRad = params['mirRad']
                self.nom = params['nom']
                self.nob = params['nob']
                if 'substrate' in params:
                    self.sub = params['substrate']
                else:
                    params['substrate'] = None
                if 'evenCL' in params:
                    self.evenCL = params['evenCL']
                else:
                    params['evenCL'] = None
                if 'oddCL' in params:
                    self.oddCL = params['oddCL']
                else:
                    params['oddCL'] = None
                if 'structure' in params:
                    self.structure = params['structure']
                else:
                    params['structure'] = None
                self.nol = params['nol']
                self.freq = params['freq']
                self.qwl = params['qwl']
                print('Creating coating stack for the noise budget...')
                self.coatStack = stack(structure=params['structure'],
                                       evenLayer=params['evenCL'],
                                       oddLayer=params['oddCL'],
                                       substrate=params['substrate'],
                                       nol=self.nol,
                                       qwl=self.qwl,
                                       lam=self.lam,
                                       lightInit=lightInit)
                self.sub = self.coatStack.getSub()
            except BaseException:
                raise RuntimeError('Dictionary params is incomplete.')

        self.fsr = scc.c/(2*self.cavLen)                    # Hz, Cavity FSR
        self.cavPole = self.fsr/(2*self.finesse)            # Hz, Cavity Pole
        # PDH slope (W/Hz):
        self.pdhSlope = 2*self.powInc*self.pdhModInd/self.cavPole
        # Conversion from length to frequency fluctuation:
        self.fConv = scc.c/(self.cavLen*self.lam)           # Hz / m
        # Spot size on mirrors:
        self.spotSize = ((self.lam*self.roc/np.pi)**0.5
                         / (2*self.roc/self.cavLen - 1)**0.25)
        # Beam waist:
        self.beamWaist = ((self.lam/(2*np.pi))**0.5
                          * (self.cavLen*(2*self.roc - self.cavLen))**0.25)
        # Thermal relaxation frequency (Hz):
        self.fTherm = self.sub.Cond/(np.pi*(self.spotSize**2)*self.sub.HeatCap)
        # Circulating power in cavities (W):
        self.circPower = self.powInc * self.finesse / np.pi
        # All frequency PSD traces will be stored in this directory as
        # [PSD, freq, 'Legend']:
        self.PSDList = {}
        # Initializing extra variables
        # Temperature PSD
        self.tempPSD = None
        # Absorbed power per beam
        self.absPower = None
        # List of calculated noises to be sumed for expected noise
        self.calcPSDList = ['coatBr', 'coatTO', 'subBr', 'subTE', 'pdhShot',
                            'pllOsc', 'pllReadout', 'seismic', 'photoThermal',
                            'resNPRO']

    def showParams(self):
        print('-------------------------------------------------------------')
        print('-------------------Noise Budget Parameters-------------------')
        print('Noise Budget Name: ', self.Name)
        print('Environmental Temperature = ', self.temp, ' K')
        print('Cavity Length = ', self.cavLen, ' m')
        print('Laser wavelength = ', self.lam, ' m')
        print('PDH Modulation Index = ', self.pdhModInd)
        print('Cavity Visibility = ', self.cavVis)
        print('Incident Power = ', self.powInc, ' W')
        print('Cavity Finesse = ', self.finesse)
        print('Radius of Curvature of Cavity Mirrors = ', self.roc, ' m')
        print('Radius of Cavity Mirrors = ', self.mirRad, ' m')
        print('Number of mirros = ', self.nom)
        print('Number of beams = ', self.nob)
        print('Number of coating layers in stack = ', self.nol)
        print('Frequency range: {0:0.4g} Hz to {1:0.4g} Hz logarithmically '
              'spaced at {2} points'.format(self.freq[0],
                                            self.freq[-1], len(self.freq)))
        print('Quarter wavelength of layers assumed?: ', self.qwl)
        if self.structure is not None:
            if isinstance(self.structure, str):
                print('Structure read from file: ', self.structure)
            else:
                print('Structure read from a dictionary. Can be written to a '
                      'file using self.coatStack.writeParams(filename)')
            print('\nSubstrate Layer: ')
            self.sub.showParams()
            print('\nFor info about coating layers, see self.coatStack')
        else:
            print('Structure constructed using alternate layers:')
            print('\nSubstrate Layer: ')
            self.sub.showParams()
            print('\nEven numbered layers: ')
            self.evenCL.showParams()
            print('Optical Thickness = ', self.coatStack.OptThick[0])
            print('Effective CTE = ', self.coatStack.CTEeff[0])
            print('Yama CTE = ', self.coatStack.CTEyama[0])

            print('\nOdd numbered layers: ')
            self.oddCL.showParams()
            print('Optical Thickness = ', self.coatStack.OptThick[1])
            print('Effective CTE = ', self.coatStack.CTEeff[1])
            print('Yama CTE = ', self.coatStack.CTEyama[1])

            print('\nThese layers can be written to files using '
                  'self.layer_name.writeParams(optionalFilename)')
        print('\nDerived parameters:')
        print('Cavity FSR: {0:.4g} GHz'.format(self.fsr/1e9))
        print('Cavity pole: {0:.3g} kHz'.format(self.cavPole/1e3))
        print('PDH slope: {0:.3g} mW/MHz'.format(self.pdhSlope * 1e9))
        print('Conversion from length to frequency fluctuation:'
              ' {0:.3g} Hz/m'.format(self.fConv))
        print('Spot size on mirrors: {0:.4g} um'.format(self.spotSize * 1e6))
        print('Beam waist: {0:.4g} um'.format(self.beamWaist * 1e6))
        print('Thermal relaxation frequency: {0:.4g} Hz'.format(self.fTherm))
        print('Circulating power in cavities:'
              ' {0:.4g} W'.format(self.circPower))

    def writeParams(self, filename=None, writeLayers=False,
                    coatStackFilename=None):
        if filename is None:
            filename = self.Name + '.yml'
        with open(filename, 'w') as f:
            f.write(fmtString(['Name: ', 11, self.Name, 34, '\n']))
            f.write(fmtString(['temp: ', 11, str(self.temp), 34,
                               '# K, Experiment Temperature\n']))
            f.write(fmtString(['cavLen: ', 11, str(self.cavLen), 34,
                               '# m, Cavity Length\n']))
            f.write(fmtString(['lam: ', 11, str(self.lam), 34,
                               '# m, Laser Wavelength\n']))
            f.write(fmtString(['pdhModInd: ', 11, str(self.pdhModInd), 34,
                               '# PDH Modulation Index\n']))
            f.write(fmtString(['cavVis: ', 11, str(self.cavVis), 34,
                               '# Cavity Visibility in fraction\n']))
            f.write(fmtString(['powInc: ', 11, str(self.powInc), 34,
                               '# W, Incident power on cavity\n']))
            f.write(fmtString(['finesse: ', 11, str(self.finesse), 34,
                               '# Cavity Finesse\n']))
            f.write(fmtString(['roc: ', 11, str(self.roc), 34,
                               '# m, Mirror Radius of Curvature\n']))
            f.write(fmtString(['mirRad: ', 11, str(self.mirRad), 34,
                               '# m, Radius of Mirrors\n']))
            f.write(fmtString(['nom: ', 11, str(self.nom), 34,
                               '# Number of Mirrors\n']))
            f.write(fmtString(['nol: ', 11, str(self.nol), 34,
                               '# Number of Layers in mirror\n']))
            f.write(fmtString(['nob: ', 11, str(self.nob), 34,
                               '# Number of beams\n']))
            f.write(fmtString(['fhigh: ', 11, str(self.freq[-1]), 34,
                               '# Hz, Frequency Axis upper limit\n']))
            f.write(fmtString(['flow: ', 11, str(self.freq[0]), 34,
                               '# Hz, Frequency Axis lower limit\n']))
            f.write(fmtString(['fnop: ', 11, str(len(self.freq)), 34,
                               '# Number of points on frequency axis\n']))
            f.write(fmtString(['qwl: ', 11, str(self.qwl), 34,
                               '# Quarter-wavelength layer thickness?\n']))
            if coatStackFilename is not None:
                print('Writing coating stack structure file at '
                      + coatStackFilename)
                self.coatStack.writeParams(coatStackFilename)
                f.write(fmtString(['structure: ', 11, coatStackFilename, 33,
                                   ' # Coating Structure File \n']))
            elif self.structure is not None:
                if isinstance(self.structure, str):
                    f.write(fmtString(['structure: ', 11, self.structure, 33,
                                       ' # Coating Structure File \n']))
                else:
                    print('Structure read from a dictionary.\n')
                    print('Writing coating stack structure file at '
                          + self.Name + '_Coating_Stack_Structure.csv')
                    self.coatStack.writeParams(self.Name
                                               + '_Coating_Stack_Structure'
                                               + '.csv')
                    structureFile = self.Name + '_Coating_Stack_Structure.csv'
                    f.write(fmtString(['structure: ', 11, structureFile, 33,
                                       ' # Coating Structure File \n']))
            else:
                subFile = self.sub.Name.replace('+/-', 'pm') + '.yml'
                evenCLFile = self.evenCL.Name.replace('+/-', 'pm') + '.yml'
                oddCLFile = self.oddCL.Name.replace('+/-', 'pm') + '.yml'
                if writeLayers:
                    if not os.path.isfile(subFile):
                        self.sub.writeParams()
                    if not os.path.isfile(evenCLFile):
                        self.evenCL.writeParams()
                    if not os.path.isfile(oddCLFile):
                        self.oddCL.writeParams()
                f.write(fmtString(['substrate: ', 11, subFile, 33,
                                   ' # Substrate Material File\n']))
                f.write(fmtString(['evenCL: ', 11, evenCLFile, 33,
                                   ' # Even Coating Layer Material File\n']))
                f.write(fmtString(['oddCL: ', 11, oddCLFile, 33,
                                   ' # Odd Coating Layer Material File\n']))

    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    # Helper Functions:
    def temperaturePSD(self):
        '''
        This function calculates PSD of temperature to be used for coating
        ThermoOptic noise calculation.
        '''
        print("Calculating temperature PSD...")
        integralM = np.zeros(len(self.freq))
        for ii in range(len(integralM)):
            integralM[ii] = scint.quad(integrandM, 0, np.inf,
                                       args=(unpnv(self.freq[ii]
                                                   / self.fTherm),
                                             )
                                       )[0]
        self.tempPSD = ((2**1.5) * scc.Boltzmann * self.temp**2 * integralM
                        / (np.pi * self.sub.Cond * self.spotSize))
        return self.tempPSD

    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    # Frequency PSD noise sources calculation functions:

    def calculateCoatingBrownianNoise(self):
        '''
        This function calculates Coating ThermoOptic noise using
        Hong et al . PRD 87, 082001 (2013).
        See stack.HongBrownianCoeffCalculations() in stack.py for
        associated parameter calculations.
        '''
        print('Using Hong et al . PRD 87, 082001 (2013) to calculate coating '
              'brownian noise.')
        print('Typical calculation time is approximately 14 min.')
        # Calculating effective beam area on each layer
        zR = (np.pi*self.beamWaist**2)/self.lam
        zk = np.zeros(self.nol+1)*uf(0, 0)
        for ii in range(1, self.nol+1, 1):
            zk[ii] = zk[ii-1]+self.coatStack.PhyThick[ii-1]
        self.coatStack.Aeff = (np.pi*(self.beamWaist**2)
                               * (1 + ((0.5*self.cavLen+zk)/zR)**2))
        del zk, zR

        # PSD at single layer with thickness equal to WaveLength
        # in the medium Eq (96)
        # Bulk
        def S_Bk(freq):
            S_Bk_num = (4*scc.Boltzmann*self.temp*self.coatStack.WaveLength
                        * self.coatStack.Loss
                        * (1 - self.coatStack.Poisson
                           - 2*self.coatStack.Poisson**2))
            S_Bk_den = (3*np.pi*freq*self.coatStack.Young
                        * ((1 - self.coatStack.Poisson)**2)
                        * self.coatStack.Aeff)
            # Leaving out substrate from return value
            return (S_Bk_num/S_Bk_den)[0:-1]
        self.S_Bk = S_Bk

        # Shear
        def S_Sk(freq):
            S_Sk_num = (4*scc.Boltzmann*self.temp*self.coatStack.WaveLength
                        * self.coatStack.LossShear
                        * (1 - self.coatStack.Poisson
                           - 2*self.coatStack.Poisson**2))
            S_Sk_den = (3*np.pi*freq*self.coatStack.Young
                        * ((1 - self.coatStack.Poisson)**2)
                        * self.coatStack.Aeff)
            # Leaving out substrate from return value
            return (S_Sk_num/S_Sk_den)[0:-1]
        self.S_Sk = S_Sk

        # z coordinates of top surfaces of layers with Substrate surface at z=0
        zk = np.zeros(self.nol+1)  # *uf(0,0)
        for ii in range(self.nol-1, -1, -1):
            zk[ii] = zk[ii+1]+self.coatStack.PhyThick[ii].nominal_value
        # Coefficients q_j from Eq (94)
        # Defining integrand functions which return nominal_value evaluations

        def integrand_q_Bk(z, j):
            return ((1/self.coatStack.WaveLength[j])
                    * ((1 - (self.coatStack.epsilon(j, z).imag*0.5))
                       * self.coatStack.C_B[j] + self.coatStack.D_B[j])**2)
        self.int_q_Bk = integrand_q_Bk

        def integrand_q_Sk(z, j):
            return ((1/self.coatStack.WaveLength[j])
                    * ((1 - (self.coatStack.epsilon(j, z).imag*0.5))
                       * self.coatStack.C_SA[j] + self.coatStack.D_SA[j])**2)

        def integrand_p_Bk(z, j):
            return ((1/self.coatStack.WaveLength[j])
                    * (self.coatStack.C_B[j]
                       * (self.coatStack.epsilon(j, z).real*0.5))**2)

        def integrand_p_Sk(z, j):
            return ((1/self.coatStack.WaveLength[j])
                    * (self.coatStack.C_SA[j]
                       * (self.coatStack.epsilon(j, z).real*0.5))**2)

        self.q_Bk = np.zeros(self.nol)*uf(0, 0)
        for jj in range(self.nol):
            self.q_Bk[jj] = uquad(integrand_q_Bk, zk[jj+1], zk[jj],
                                  args=(jj,))[0]

        self.q_Sk = np.zeros(self.nol)*uf(0, 0)
        for jj in range(self.nol):
            self.q_Sk[jj] = (uquad(integrand_q_Sk, zk[jj+1], zk[jj],
                                   args=(jj,))[0]
                             + ((self.coatStack.PhyThick[jj]
                                 / self.coatStack.WaveLength[jj])
                                * self.coatStack.D_SB[jj]**2))

        self.S_Xi = np.zeros(len(self.freq))*uf(0, 0)
        for ii in range(len(self.freq)):
            self.S_Xi[ii] = np.sum(self.q_Bk*S_Bk(self.freq[ii])
                                   + self.q_Sk*S_Sk(self.freq[ii]))
        # Taking nominal_value for now.
        # Will implement uncertainties fully in future.

        self.p_Bk = np.zeros(self.nol)*uf(0, 0)
        for jj in range(self.nol):
            self.p_Bk[jj] = uquad(integrand_p_Bk, zk[jj+1], zk[jj],
                                  args=(jj,))[0]

        self.p_Sk = np.zeros(self.nol)*uf(0, 0)
        for jj in range(self.nol):
            self.p_Sk[jj] = uquad(integrand_p_Sk, zk[jj+1], zk[jj],
                                  args=(jj,))[0]

        self.S_Zeta = np.zeros(len(self.freq))*uf(0, 0)
        for ii in range(len(self.freq)):
            self.S_Zeta[ii] = np.sum(self.p_Bk*S_Bk(self.freq[ii])
                                     + self.p_Sk*S_Sk(self.freq[ii]))
        # Taking nominal_value for now.
        # Will implement uncertainties fully in future.

        # Will change this later. This is for mirror of infinite mass.
        ampNoise_to_dispNoise = 0
        self.PSDList['coatBr'] = [(self.nom*(self.fConv**2)
                                   * (unp.sqrt(self.S_Xi)
                                      + ampNoise_to_dispNoise
                                      * unp.sqrt(self.S_Zeta))**2),
                                  self.freq, 'Coating Brownian Noise']
        self.PSDList['coatBrPh'] = [self.nom*(self.fConv**2)*self.S_Xi,
                                    self.freq,
                                    'Coating Brownian Noise via Phase Noise']
        self.PSDList['coatBrAmp'] = [self.nom*(self.fConv**2)*self.S_Zeta,
                                     self.freq,
                                     ('Coating Brownian Noise via '
                                      + 'Amplitude Noise')]

        return self.PSDList['coatBr'][0]

    def calculateCoatingThermoOpticNoise(self):
        '''
        This function calculates Coating ThermoOptic noise using
        Evans et al . PRD 78, 102003 (2008).
        See stack.EvanThermoOpticCoeffCalculations() in stack.py for
        associated parameter calculations.
        '''
        print('Using Evans et al. PRD 78, 102003 (2008) to calculate '
              'coating thermo-optic noise.')
        if self.tempPSD is None:
            self.temperaturePSD()
        # Following is from Evans et al. PRD 78, 102003 (2008)
        # for ThermoOptic noise
        # Thick coating correction factors (Using 39)
        coatHeatCap = self.coatStack.coatHeatCap
        coatCond = self.coatStack.coatCond
        subHeatCap = self.sub.HeatCap
        subCond = self.sub.Cond
        coatThickness = np.sum(self.coatStack.PhyThick[0:-1])
        delCTE = self.coatStack.delCTE
        coatCTR = self.coatStack.coatCTR
        # Eq (36)
        rTherm = unp.sqrt(coatCond * coatHeatCap / (subCond * subHeatCap))
        # Eq (41)
        xi = unp.sqrt(4*np.pi*self.freq*coatHeatCap / coatCond) * coatThickness
        # Eq (42)
        pR = -coatCTR*self.lam / (delCTE*coatThickness - coatCTR*self.lam)
        # Eq (42)
        pE = delCTE*coatThickness / (delCTE*coatThickness - coatCTR*self.lam)
        Gamma0 = 2 * (unp.sinh(xi) - unp.sin(xi)) + 2*rTherm * (unp.cosh(xi)
                                                                - unp.cos(xi))
        Gamma1 = 8 * unp.sin(xi/2) * (rTherm*unp.cosh(xi/2) + unp.sinh(xi/2))
        Gamma2 = ((1 + rTherm**2) * unp.sinh(xi)
                  + (1 - rTherm**2) * unp.sin(xi)
                  + 2*rTherm * unp.cosh(xi))
        GammaD = ((1 + rTherm**2) * unp.cosh(xi)
                  + (1 - rTherm**2) * unp.cos(xi)
                  + 2*rTherm * unp.sinh(xi))
        self.GammaTC = ((pE**2 * Gamma0 + pE*pR*xi*Gamma1
                         + pR**2 * xi**2 * Gamma2)
                        / (rTherm * xi**2 * GammaD))

        # Calculating Thermo-refractive, thermoelastice
        # and net ThermoOptic noise
        # Note here, (fConv**2) is multiplied to convert from displacement PSD
        #            to frequency PSD
        #            nom is multiplied for coating noises in different mirrors
        #            add up in quadrature
        # All in # m**2/Hz
        self.PSDList['coatTO'] = [(self.nom*(self.fConv**2)*self.tempPSD
                                   * self.GammaTC
                                   * (delCTE*coatThickness
                                      - coatCTR*self.lam)**2),
                                  self.freq, 'Coating Thermo-optic']
        self.PSDList['coatTE'] = [(self.nom*(self.fConv**2)*self.tempPSD
                                   * self.GammaTC
                                   * (delCTE*coatThickness)**2),
                                  self.freq, 'Coating Thermo-elastic']
        self.PSDList['coatTR'] = [(self.nom*(self.fConv**2)*self.tempPSD
                                   * self.GammaTC * (coatCTR*self.lam)**2),
                                  self.freq, 'Coating Thermo-refractive']

        return self.PSDList['coatTO'][0]

    def calculateSubstrateBrownianNoise(self):
        '''
        This function calculates Substrate Brownian Noise Using
        Cole et al. Nature Photonics 7, 644–650 (2013) Eq.1.
        '''
        print('Using  Cole et al. Nature Photonics 7, 644–650 (2013) Eq.1'
              ' to calculate Substrate Brownian Noise.')
        # Note here, (fConv**2) is multiplied to convert from displacement PSD
        #            to frequency PSD
        #            nom is multiplied for coating noises in different mirrors
        #            add up in quadrature
        # To do: We can add finite test mass correction factor here from
        #       Liu and Thorne PRD 62 122002 (2000)
        #       Also, can be seen in concise in Ch 7 of "Optical Coatings And
        #       Thermal Noise in Precision Measurement"
        #       https://doi.org/10.1017/CBO9780511762314.009
        self.PSDList['subBr'] = [(self.nom*(self.fConv**2)
                                  * ((2*scc.Boltzmann*self.temp)
                                     / (np.pi**1.5 * self.freq))
                                  * (1-self.sub.Poisson**2)
                                  * self.sub.Loss
                                  / (self.spotSize * self.sub.Young)),
                                 self.freq, 'Substrate Brownian']
        return self.PSDList['subBr'][0]

    def calculateSubstrateThermoElasticNoise(self):
        '''
        This function calculates Substrate ThermoElastic Noise Using
        Somiya et al PRD 82, 127101 (2010) Eq (3) and (8)
        (Analytical expression for the theory by Cerdonio et al. (2001))
        '''
        print('Using Somiya et al. (2010) Eq (3) and (8) to calculate '
              'Substrate ThermoElastic Noise.')
        Omega = unpnv(self.freq/self.fTherm)
        J = (- np.real((np.exp(1j*Omega/2)*(1-1j*Omega)/(Omega**2))
                       * scsp.erfc(np.sqrt(Omega)*(1+1j)/2))
             + Omega**-2
             - (np.pi*Omega**3)**-0.5)
        # Note here, (fConv**2) is multiplied to convert from displacement PSD
        #            to frequency PSD
        #            nom is multiplied for coating noises in different mirrors
        #            add up in quadrature
        # To do: We can add finite test mass correction factor here from
        #       Liu and Thorne PRD 62 122002 (2000)
        #       Also, can be seen in concise in Ch 7 of "Optical Coatings And
        #       Thermal Noise in Precision Measurement"
        #       https://doi.org/10.1017/CBO9780511762314.009
        self.PSDList['subTE'] = [(self.nom * (self.fConv**2)
                                  * (4*scc.Boltzmann * (self.temp**2)
                                     / np.sqrt(np.pi))
                                  * (self.sub.CTE**2 * (1+self.sub.Poisson)**2
                                     * self.spotSize/self.sub.Cond)
                                  * J), self.freq, 'Substrate Thermo-elastic']
        return self.PSDList['subTE'][0]

    def calculatePDHShotNoise(self):
        '''
        This function calculates PDH shot noise for given visiblity and
        PDH modulation index.
        '''
        # We'll use unc.wrap to make uncertainties-compatible Bessel functions
        wrapj0 = unc.wrap(scsp.j0)
        wrapj1 = unc.wrap(scsp.j1)
        # Compute the shot noise PSD in (W^2/Hz)
        pdhShotPSDpow = (2 * scc.Planck * scc.c / self.lam * self.powInc
                         * (wrapj0(self.pdhModInd)**2 * (1-self.cavVis)
                            + 3*wrapj1(self.pdhModInd)**2))
        # Now account for the cavity pole and convert from power to frequency
        pdhShotPSDfreq = (pdhShotPSDpow * (1+(self.freq/self.cavPole)**2)
                          / self.pdhSlope**2)
        # Note here, nob is multiplied for pdh noise in different beams add
        #            in quadrature.
        self.PSDList['pdhShot'] = [self.nob*pdhShotPSDfreq,
                                   self.freq, 'PDH Shot Noise']
        return self.PSDList['pdhShot'][0]

    def updatePDHShotNoise(self, Pinc):
        if Pinc is not None:
            if isinstance(Pinc, list):
                if len(Pinc) != self.nob:
                    if len(Pinc) == 1:
                        Pinc = [Pinc[0] for ii in range(self.nob)]
                    else:
                        raise RuntimeError('Pinc must be a list same in '
                                           'length as number of beams')
            else:
                Pinc = [Pinc for ii in range(self.nob)]
        Pinc = np.array(Pinc)
        pdhShotPSDfreq = self.PSDList['pdhShot'][0]/self.nob
        newTotalPDHShotNoise = np.zeros_like(pdhShotPSDfreq)
        for ii in range(self.nob):
            newTotalPDHShotNoise += pdhShotPSDfreq * self.powInc / Pinc[ii]
        self.PSDList['pdhShot'] = [newTotalPDHShotNoise,
                                   self.freq, 'PDH Shot Noise']
        return self.PSDList['pdhShot'][0]

    def calculatePhotoThermalNoise(self, RINfiles, coatAbs):
        '''
        This function calculated Photothermal noise in the mirrors using
        Farsi et al . JoAPh 111, 043101 (2012)
        It requires
        .txt files of RIN measurments of the lasers involved.
        Input arguments:
        RINfiles: List of filenames (with path) containing RIN measurment
                  of each laser.
        coatAbs: If list, each value correspond to coating absorptivity of
                 corresponding mirrors where the beam whose RIN is measured
                 is present.
                 If a single value, same coating absorptivity is used
                 everywhere.
        File format required:
        # Atleast three columns
        # Frequency | RIN_value | RIN_std
           ...  ...  ...
        File can be .txt (white space separated) or .csv (for comma separated)
        '''
        print('Using Farsi et al . JoAPh 111, 043101 (2012) to calculate',
              ' Photothermal noise.')
        print('Wait 1 to 2 minutes ...')
        # Local substrate variables for ease in coding.
        coatHeatCap = self.coatStack.coatHeatCap
        coatCond = self.coatStack.coatCond

        # See CTN:2388
        # coatEffCTE = self.coatStack.coatCTE # Ideal and consistent def
        coatEffCTE = np.sum(self.coatStack.CTE[0:-1]
                            * self.coatStack.PhyThick[0:-1]
                            / self.coatStack.coatPhyThick)

        coatCTR = self.coatStack.coatCTR
        coatPoisson = self.coatStack.Poisson[0]  # Taking first layers value.
        # This should be more complicated ideally.
        coatYoung = self.coatStack.Young[0]  # Same note as above
        dNom = (np.sum(self.coatStack.PhyThick[0:-1])).nominal_value
        subHeatCap = self.sub.HeatCap
        subCond = self.sub.Cond
        subCTE = self.sub.CTE
        subPoisson = self.sub.Poisson
        subYoung = self.sub.Young

        # Coefficients for calculations
        self.PTgamma1 = unpnv(0.5 * (1+coatPoisson)
                              / (1-coatPoisson)
                              * (1 + (1-2*subPoisson)
                                 * (1+subPoisson)/(1+coatPoisson)
                                 * coatYoung / subYoung))
        self.PTgamma2 = unpnv((1-subPoisson**2) / (1-coatPoisson)
                              * coatYoung / subYoung)
        self.PTr0 = r0 = self.spotSize.nominal_value/np.sqrt(2)
        self.PTf_c = unpnv(coatCond / (2 * np.pi * coatHeatCap * r0**2))
        self.PTf_s = unpnv(subCond / (2 * np.pi * subHeatCap * r0**2))

        # Some function definitions for Eq A44. Note all functions are saved
        # as class parameters as well for outside access.
        def kCoat(ff):
            return np.sqrt(2j * unpnv(np.pi * ff * coatHeatCap / coatCond))
        self.PTkCoat = kCoat

        def kSub(ff):
            return np.sqrt(2j * unpnv(np.pi * ff * subHeatCap / subCond))
        self.PTkSub = kSub

        def fancyR(xi, ff):
            return (unpnv(coatCond/subCond)
                    * np.sqrt((r0**2 * kCoat(ff)**2 + xi**2)
                              / (r0**2 * kSub(ff)**2 + xi**2)))
        self.PTfancyR = fancyR

        def xiCoat(xi, ff):
            return np.sqrt(1j * ff / self.PTf_c + xi**2)
        self.PTxiCoat = xiCoat

        def xiSub(xi, ff):
            return np.sqrt(1j * ff / self.PTf_s + xi**2)
        self.PTxiSub = xiSub

        def Ffunc(xi, ff):
            return (np.cosh(dNom / r0 * xiCoat(xi, ff))
                    + fancyR(xi, ff) * np.sinh(dNom / r0 * xiCoat(xi, ff)))**-1
        self.PTFfunc = Ffunc

        def G1func(xi, ff):
            return (np.cosh(xi * dNom / r0)
                    + fancyR(xi, ff) * xi/xiCoat(xi, ff) * np.sinh(xi*dNom/r0)
                    - np.cosh(dNom/r0*xiCoat(xi, ff))
                    - fancyR(xi, ff) * np.sinh(dNom/r0*xiCoat(xi, ff)))
        self.PTG1func = G1func

        def G2func(xi, ff):
            return (fancyR(xi, ff) * np.cosh(xi*dNom/r0)
                    + xiCoat(xi, ff)/xi * np.sinh(xi*dNom/r0)
                    - fancyR(xi, ff) * np.cosh(dNom/r0*xiCoat(xi, ff))
                    - np.sinh(dNom/r0*xiCoat(xi, ff)))
        self.PTG2func = G2func

        def integrandPTCoat(xi, ff):
            return (xi * np.exp(-xi**2/2) * Ffunc(xi, ff)
                    * (self.PTgamma1 * G1func(xi, ff)
                       - self.PTgamma2*xi/xiCoat(xi, ff) * G2func(xi, ff)))
        self.PTintegrand_c = integrandPTCoat

        def integrandPTCoatReal(xi, ff):
            return np.real(integrandPTCoat(xi, ff))
        self.PTintegrand_cReal = integrandPTCoatReal

        def integrandPTCoatImag(xi, ff):
            return np.imag(integrandPTCoat(xi, ff))
        self.PTintegrand_cImag = integrandPTCoatImag

        def integrandPTSub(xi, ff):
            return xi * np.exp(-xi**2/2) * Ffunc(xi, ff) \
                * (1 - xi/xiSub(xi, ff))
        self.PTintegrand_s = integrandPTSub

        def integrandPTSubReal(xi, ff):
            return np.real(integrandPTSub(xi, ff))
        self.PTintegrand_sReal = integrandPTSubReal

        def integrandPTSubImag(xi, ff):
            return np.imag(integrandPTSub(xi, ff))
        self.PTintegrand_sImag = integrandPTSubImag

        def integrandPTTR(xi, ff):
            return (xi*np.exp(-xi**2/2)/xiCoat(xi, ff)
                    * (np.sinh(dNom/r0*xiCoat(xi, ff))
                       + fancyR(xi, ff) * np.cosh(dNom/r0*xiCoat(xi, ff)))
                    / (np.cosh(dNom/r0*xiCoat(xi, ff))
                       + fancyR(xi, ff) * np.sinh(dNom/r0*xiCoat(xi, ff))))
        self.PTintegrand_tr = integrandPTTR

        def integrandPTTRReal(xi, ff):
            return np.real(integrandPTTR(xi, ff))
        self.PTintegrand_trReal = integrandPTTRReal

        def integrandPTTRImag(xi, ff):
            return np.imag(integrandPTTR(xi, ff))
        self.PTintegrand_trImag = integrandPTTRImag

        # Doing integral for PT transfer function due to coating Eq A44
        integralPTCoat = np.zeros(len(self.freq), dtype=np.complex)
        integralPTCoatErr = np.zeros(len(self.freq), dtype=np.complex)
        for ii in range(len(self.freq)):
            thisReal, thisRealErr = scint.quad(integrandPTCoatReal, 0, np.inf,
                                               args=(self.freq[ii],),
                                               epsabs=1e-8, epsrel=1e-5,
                                               limit=20)[:2]
            thisImag, thisImagErr = scint.quad(integrandPTCoatImag, 0, np.inf,
                                               args=(self.freq[ii],),
                                               epsabs=1e-8, epsrel=1e-5,
                                               limit=20)[:2]
            integralPTCoat[ii] = thisReal + 1j*thisImag
            integralPTCoatErr[ii] = thisRealErr + 1j*thisImagErr
        self.PTCoatTF = (unpnv(coatEffCTE / (np.pi*coatCond)
                               * self.PTf_c)
                         / (1j * self.freq) * integralPTCoat)

        # Doing integral for PT transfer function due to substrate Eq A45
        integralPTSub = np.zeros(len(self.freq), dtype=np.complex)
        integralPTSubErr = np.zeros(len(self.freq), dtype=np.complex)
        for ii in range(len(integralPTSub)):
            thisReal, thisRealErr = scint.quad(integrandPTSubReal, 0, np.inf,
                                               args=(self.freq[ii],),
                                               epsabs=1e-3, epsrel=1e-3,
                                               limit=20)[:2]
            thisImag, thisImagErr = scint.quad(integrandPTSubImag, 0, np.inf,
                                               args=(self.freq[ii],),
                                               epsabs=1e-3, epsrel=1e-3,
                                               limit=20)[:2]
            integralPTSub[ii] = thisReal + 1j*thisImag
            integralPTSubErr[ii] = thisRealErr + 1j*thisImagErr
        self.PTSubTF = - (unpnv(subCTE * (1 + subPoisson)
                                / (np.pi * subCond) * self.PTf_s)
                          / (1j * self.freq) * integralPTSub)

        # Doing integral for PT transfer function due to thermorefractive
        # effect Eq A49
        integralPTTR = np.zeros(len(self.freq), dtype=np.complex)
        integralPTTRErr = np.zeros(len(self.freq), dtype=np.complex)
        for ii in range(len(integralPTCoat)):
            thisReal, thisRealErr = scint.quad(integrandPTTRReal, 0, np.inf,
                                               args=(self.freq[ii],),
                                               epsabs=1, epsrel=1e-2,
                                               limit=20)[:2]
            thisImag, thisImagErr = scint.quad(integrandPTTRImag, 0, np.inf,
                                               args=(self.freq[ii],),
                                               epsabs=1, epsrel=1e-2,
                                               limit=20)[:2]
            integralPTTR[ii] = thisReal + 1j*thisImag
            integralPTTRErr[ii] = thisRealErr + 1j*thisImagErr
        self.PTtrTF = (unpnv(self.lam*coatCTR
                             / (2*np.pi*coatCond*self.PTr0))
                       * integralPTTR)

        # Total Photothermal transfer functions
        self.PTTF = self.PTCoatTF + self.PTSubTF + self.PTtrTF

        # Loading RIN measurements to calculate PSD
        RIN_PSD = np.zeros((len(self.freq), len(RINfiles)))*uf(0, 0)
        RIN_PSD_bnds = []
        for ii, fn in enumerate(RINfiles):
            dm = None
            if '.csv' in fn:
                dm = ','
            if np.shape(np.loadtxt(fn, delimiter=dm))[1] == 3:
                stdFlag = True
                fRIN, nomRIN, stdRIN = np.loadtxt(fn, unpack=1, delimiter=dm,
                                                  usecols=(0, 1, 2))
                nomRIN = np.interp(self.freq, fRIN, nomRIN)
                stdRIN = np.interp(self.freq, fRIN, stdRIN)
                RIN_PSD[:, ii] = unp.uarray(nomRIN, stdRIN)
            elif np.shape(np.loadtxt(fn, delimiter=dm))[1] == 4:
                stdFlag = False
                fRIN, medRIN, RINlb, RINub = np.loadtxt(fn, unpack=1,
                                                        delimiter=dm,
                                                        usecols=(0, 1, 2, 3))
                medRIN_PSD = np.interp(self.freq, fRIN, medRIN**2)
                RINlb_PSD = np.interp(self.freq, fRIN, RINlb**2)
                RINub_PSD = np.interp(self.freq, fRIN, RINub**2)
                RIN_PSD_bnds += [[medRIN_PSD, RINlb_PSD, RINub_PSD]]

        # If a single coating absorptivity is provided, it is used for all
        # If coatAbs is an array, it is used for respective RINfile in the
        # order provided.

        if type(coatAbs) is list:
            if len(RINfiles) != len(coatAbs):
                raise RuntimeError('coatAbs must be a list same in length'
                                   ' as RINfiles')
            self.absPower = np.zeros(len(RINfiles))*uf(0, 0)
            for ii in range(len(RINfiles)):
                self.absPower[ii] = coatAbs[ii] * self.circPower
        else:
            self.absPower = np.ones(len(RINfiles)) * coatAbs * self.circPower

        if stdFlag:
            self.PT_PSD = np.zeros((len(self.freq), len(RINfiles)))*uf(0, 0)
            totalPTPSD = np.zeros(len(self.freq))*uf(0, 0)
            for ii in range(len(RINfiles)):
                self.PT_PSD[:, ii] = (RIN_PSD[:, ii] * self.absPower[ii]**2
                                      * np.abs(self.PTTF)**2 * self.fConv**2)
                totalPTPSD = totalPTPSD + self.PT_PSD[:, ii]
            self.PSDList['photoThermal'] = [totalPTPSD, self.freq,
                                            'Photothermal Noise']
        else:
            self.PT_PSD = np.zeros((len(self.freq), len(RINfiles)))
            self.PT_PSD_lb = np.zeros((len(self.freq), len(RINfiles)))
            self.PT_PSD_ub = np.zeros((len(self.freq), len(RINfiles)))
            totalPTPSD = np.zeros(len(self.freq))
            totalPTPSD_lb = np.zeros(len(self.freq))
            totalPTPSD_ub = np.zeros(len(self.freq))
            for ii, RIN_PSD_b in enumerate(RIN_PSD_bnds):
                self.PT_PSD[:, ii] = unpnv(RIN_PSD_b[0]
                                           * self.absPower[ii]**2
                                           * np.abs(self.PTTF)**2
                                           * self.fConv**2)
                self.PT_PSD_lb[:, ii] = (unpnv(RIN_PSD_b[1]
                                               * self.absPower[ii]**2
                                               * np.abs(self.PTTF)**2
                                               * self.fConv**2)
                                         - unpstd(RIN_PSD_b[1]
                                                  * self.absPower[ii]**2
                                                  * np.abs(self.PTTF)**2
                                                  * self.fConv**2))
                self.PT_PSD_ub[:, ii] = (unpnv(RIN_PSD_b[2]
                                               * self.absPower[ii]**2
                                               * np.abs(self.PTTF)**2
                                               * self.fConv**2)
                                         + unpstd(RIN_PSD_b[2]
                                                  * self.absPower[ii]**2
                                                  * np.abs(self.PTTF)**2
                                                  * self.fConv**2))
                totalPTPSD = totalPTPSD + self.PT_PSD[:, ii]
                totalPTPSD_lb = totalPTPSD_lb + self.PT_PSD_lb[:, ii]
                totalPTPSD_ub = totalPTPSD_ub + self.PT_PSD_ub[:, ii]
            self.PSDList['photoThermal'] = [totalPTPSD, self.freq,
                                            'Photothermal Noise',
                                            totalPTPSD_lb, totalPTPSD_ub]
        self.PSDList['PT_wo_RIN_coatAbs'] = [(self.circPower**2
                                              * np.abs(self.PTTF)**2
                                              * self.fConv**2),
                                             self.freq,
                                             'PT_wo_RIN_coatAbs']
        return totalPTPSD

    def updatePhotoThermalNoise(self, RINdata, coatAbs, nob=2, Pcirc=None):
        if 'PT_wo_RIN_coatAbs' not in self.PSDList:
            raise RuntimeError('Propotionality of Photo themal noise to RIN '
                               'has not been calculated yet.')
        updatePcirc = False
        if Pcirc is not None:
            updatePcirc = True
            if isinstance(Pcirc, list):
                if len(Pcirc) != nob:
                    if len(Pcirc) == 1:
                        Pcirc = [Pcirc[0] for ii in range(nob)]
                    else:
                        raise RuntimeError('Pcirc must be a list same in '
                                           'length as number of beams')
            else:
                Pcirc = [Pcirc for ii in range(nob)]
        ncol = np.shape(RINdata)[1]
        if isinstance(coatAbs, list) or isinstance(coatAbs, np.ndarray):
            if len(coatAbs) != nob:
                if len(coatAbs) == 1:
                    coatAbs = [coatAbs[0] for ii in range(nob)]
                else:
                    raise RuntimeError('coatAbs must be a list same in length'
                                       ' as number of beams')
        else:
            coatAbs = [coatAbs for ii in range(nob)]

        if ncol == nob+1:
            print('Assuming RIN Data without uncertainties')
            self.PT_PSD = np.zeros((len(self.freq), nob))*uf(0, 0)
            totalPTPSD = np.zeros(len(self.freq))*uf(0, 0)
            for ii in range(nob):
                RIN_PSD = np.interp(self.freq, RINdata[:, 0],
                                    RINdata[:, ii+1]**2)
                self.PT_PSD[:, ii] = (self.PSDList['PT_wo_RIN_coatAbs'][0]
                                      * coatAbs[ii]**2 * RIN_PSD)
                if updatePcirc:
                    self.PT_PSD[:, ii] *= (Pcirc[ii]**2) / (self.circPower**2)
                totalPTPSD = totalPTPSD + self.PT_PSD[:, ii]

            self.PSDList['photoThermal'] = [totalPTPSD, self.freq,
                                            'Photothermal Noise']
        elif ncol == 2*nob+1:
            print('Assuming RIN Data with standard deviations')
            self.PT_PSD = np.zeros((len(self.freq), nob))*uf(0, 0)
            totalPTPSD = np.zeros(len(self.freq))*uf(0, 0)
            for ii in range(nob):
                RIN_PSD = unp.uarray(np.interp(self.freq, RINdata[:, 0],
                                               RINdata[:, 2*ii+1]**2),
                                     np.interp(self.freq, RINdata[:, 0],
                                               RINdata[:, 2*ii+2]**2))
                self.PT_PSD[:, ii] = (self.PSDList['PT_wo_RIN_coatAbs'][0]
                                      * coatAbs[ii]**2 * RIN_PSD)
                if updatePcirc:
                    self.PT_PSD[:, ii] *= (Pcirc[ii]**2) / (self.circPower**2)
                totalPTPSD = totalPTPSD + self.PT_PSD[:, ii]
            self.PSDList['photoThermal'] = [totalPTPSD, self.freq,
                                            'Photothermal Noise']
        elif ncol == 3*nob+1:
            print('Assuming RIN Data with median and upper/lower bounds.')
            self.PT_PSD = np.zeros((len(self.freq), nob))
            self.PT_PSD_lb = np.zeros((len(self.freq), nob))
            self.PT_PSD_ub = np.zeros((len(self.freq), nob))
            totalPTPSD = np.zeros(len(self.freq))
            totalPTPSD_lb = np.zeros(len(self.freq))
            totalPTPSD_ub = np.zeros(len(self.freq))
            for ii in range(nob):
                RIN_PSD_b = [np.interp(self.freq, RINdata[:, 0],
                                       RINdata[:, 3*ii+1]**2),
                             np.interp(self.freq, RINdata[:, 0],
                                       RINdata[:, 3*ii+2]**2),
                             np.interp(self.freq, RINdata[:, 0],
                                       RINdata[:, 3*ii+3]**2)]
                propFac = self.PSDList['PT_wo_RIN_coatAbs'][0]
                if updatePcirc:
                    propFac *= (Pcirc[ii]**2) / (self.circPower**2)
                self.PT_PSD[:, ii] = unpnv(propFac * coatAbs[ii]**2
                                           * RIN_PSD_b[0])
                self.PT_PSD_lb[:, ii] = (unpnv(propFac * coatAbs[ii]**2
                                               * RIN_PSD_b[1])
                                         - unpstd(propFac * coatAbs[ii]**2
                                                  * RIN_PSD_b[1]))
                self.PT_PSD_ub[:, ii] = (unpnv(propFac * coatAbs[ii]**2
                                               * RIN_PSD_b[2])
                                         + unpstd(propFac * coatAbs[ii]**2
                                                  * RIN_PSD_b[2]))
                totalPTPSD = totalPTPSD + self.PT_PSD[:, ii]
                totalPTPSD_lb = totalPTPSD_lb + self.PT_PSD_lb[:, ii]
                totalPTPSD_ub = totalPTPSD_ub + self.PT_PSD_ub[:, ii]
            self.PSDList['photoThermal'] = [totalPTPSD, self.freq,
                                            'Photothermal Noise',
                                            totalPTPSD_lb, totalPTPSD_ub]
        else:
            raise RuntimeError('Data not understood.')

    def calculateTotalEstNoise(self):
        totalPSD = np.zeros(len(self.freq))
        totalPSDlb = np.zeros(len(self.freq))
        totalPSDub = np.zeros(len(self.freq))
        for key in self.calcPSDList:
            if key in self.PSDList:
                PSD = self.PSDList[key][0]
                if isinstance(PSD[0], unc.core.AffineScalarFunc):
                    totalPSD = totalPSD + unpnv(PSD)
                    totalPSDlb = totalPSDlb + unpnv(PSD) - unpstd(PSD)
                    totalPSDub = totalPSDub + unpnv(PSD) + unpstd(PSD)
                elif len(self.PSDList[key]) == 5:
                    totalPSD = totalPSD + PSD
                    totalPSDlb = totalPSDlb + self.PSDList[key][3]
                    totalPSDub = totalPSDub + self.PSDList[key][4]
                else:
                    totalPSD = totalPSD + PSD
                    totalPSDlb = totalPSDlb + PSD
                    totalPSDub = totalPSDub + PSD
        self.PSDList['total'] = [totalPSD, self.freq,
                                 'Total expected noise',
                                 totalPSDlb, totalPSDub]
        return totalPSD

    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    # Plotting Functions:
    def plotOptThickProfile(self, **kwargs):
        fig = plt.figure(figsize=(16, 12))
        CoatingOptThick = self.coatStack.OptThick[:-1]
        thelen = np.arange(1, len(CoatingOptThick)+1)
        ax = fig.gca()
        # If coating stack is defined by even and odd layers:
        if self.structure is None:
            cc1 = (0.1, 0.1, 0.7)
            cc2 = (0.7, 0.1, 0.1)
            ax.bar(thelen[0::2],
                   unpnv(CoatingOptThick[0::2]/self.lam),
                   color=cc1, **kwargs)
            ax.bar(thelen[1::2],
                   unpnv(CoatingOptThick[1::2]/self.lam),
                   color=cc2, **kwargs)
            ax.legend([self.evenCL.Name, self.oddCL.Name])
        else:
            layNames = self.coatStack.matName[:-1]
            cList = ['#e6194b', '#4363d8', '#f58231', '#3cb44b', '#ffe119',
                     '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe',
                     '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000',
                     '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080',
                     '#ffffff', '#000000']
            cno = len(cList)
            distinctMaterials = []
            for ln in layNames:
                if ln not in distinctMaterials:
                    distinctMaterials.append(ln)
            doneLabelList = []
            for jj in range(self.nol):
                for ii, dn in enumerate(distinctMaterials):
                    if layNames[jj] == dn:
                        if dn not in doneLabelList:
                            ax.bar(jj+1,
                                   unpnv(CoatingOptThick[jj] / self.lam),
                                   color=cList[np.mod(ii, cno)],
                                   label=dn,
                                   **kwargs)
                            doneLabelList += [dn]
                        else:
                            ax.bar(jj+1,
                                   unpnv(CoatingOptThick[jj] / self.lam),
                                   color=cList[np.mod(ii, cno)],
                                   **kwargs)
            ax.legend()
        ax.set_xlabel('Layer number')
        ax.set_ylabel(r'Optical thickness ($\lambda$)')
        ax.set_title(self.Name+': Optical Thickness of different layers')
        return fig

    def plotPSD(self, plotList=None, title=None, filename=None, doTotal=True,
                savePlot=True, displacementUnits=False, singleYaxis=False,
                **kwargs):
        '''
        This function plots the PSDs from the PSDList.
        Input arguments:
            plotList: List of keys of self.PSDList to be plotted. If None, only
                        PDS from self.calcPSDList will be plotted.
            title: Title of the plot
            filename: If provided, figure will be saved with this filename
            doTotal: If true, total expected noise is calculated and plotted.
            savePlot: If true, plot will be saved.
            displacementUnits: If true., units of m/rtHz will be used.
            **kwargs: Any arguments (color, linestyle, label, etc.) to pass
                to the plot
        Output: Handle to the figure generated.
        '''

        if plotList is None:
            plotList = self.calcPSDList
        if displacementUnits:
            fac = 1 / self.fConv.nominal_value / np.sqrt(self.nom)
            ylabel_l = r'ASD of Coating Displacement Noise $[m/Hz^{1/2}]$'
            ylabel_r = r'ASD of beat note frequency $[Hz/Hz^{1/2}]$'

            if title is None:
                title = 'Coating Displacement Noise ASD '+self.Name
        else:
            fac = 1
            ylabel_l = r'ASD of beat note frequency $[Hz/Hz^{1/2}]$'
            ylabel_r = r'ASD of Coating Displacement Noise $[m/Hz^{1/2}]$'
            if title is None:
                title = 'Frequency Noise ASD '+self.Name
        if doTotal:
            self.calculateTotalEstNoise()
            if 'total' not in plotList:
                plotList += ['total']

        cList = ['#800000', '#9a6324', '#808000', '#000075', '#e6194b',
                 '#f58231', '#3cb44b', '#4363d8', '#911eb4', '#f032e6']
        cno = len(cList)
        lsList = ['-', '--', '-.']
        lno = len(lsList)

        psdPlot = plt.figure(figsize=(16, 12))
        ax = psdPlot.add_subplot(111)
        for ii, psd in enumerate(plotList):
            if psd in self.PSDList:
                color = cList[np.mod(ii, cno)]
                lineStyle = lsList[np.mod(ii, lno)]
                if psd == 'total':
                    color = '#000000'
                    lineStyle = '-'
                if isinstance(self.PSDList[psd][0][0],
                              unc.core.AffineScalarFunc):
                    PSD = self.PSDList[psd][0]
                    ASD = unpnv(unp.sqrt(PSD))
                    fillFloor = np.sqrt(np.max([unpnv(PSD) - unpstd(PSD),
                                                np.ones(len(PSD))*1e-12],
                                               axis=0))
                    fillCieling = np.sqrt(unpnv(PSD) + unpstd(PSD))
                    ax.fill_between(self.PSDList[psd][1], fillFloor * fac,
                                    fillCieling * fac, alpha=0.2,
                                    color=color,
                                    lw=0, **kwargs)
                else:
                    ASD = np.sqrt(self.PSDList[psd][0])
                    if len(self.PSDList[psd]) == 5:
                        fillFloor = np.sqrt(np.max([self.PSDList[psd][3],
                                                    np.ones(len(ASD))*1e-12],
                                                   axis=0))
                        fillCieling = np.sqrt(self.PSDList[psd][4])
                        ax.fill_between(self.PSDList[psd][1], fillFloor * fac,
                                        fillCieling * fac, alpha=0.2,
                                        color=color,
                                        lw=0, **kwargs)
                ax.loglog(self.PSDList[psd][1], ASD * fac,
                          label=self.PSDList[psd][2],
                          alpha=0.8, c=color,
                          linestyle=lineStyle,
                          lw=1.5, **kwargs)
            else:
                print(psd+' has not been calculated yet.')
        ax.set_xlabel('Frequency [Hz]')
        ax.set_ylabel(ylabel_l)
        ax.legend(loc='best', fontsize=18, numpoints=2, borderpad=0.1,
                  ncol=2, columnspacing=0.5, framealpha=0.7)
        ax.grid(linestyle='solid', which='major', alpha=0.75)
        ax.grid(linestyle='dashed', which='minor', alpha=0.25)
        ylimits = [6e-5 * fac, 2e3 * fac]
        ax.set_ylim(ylimits[0], ylimits[1])
        ax.set_title(title)

        if not singleYaxis:
            def Hztom(x):
                return x / self.fConv.n / np.sqrt(self.nom)

            def mtoHz(x):
                return x * self.fConv.n * np.sqrt(self.nom)

            if displacementUnits:
                secaxy = ax.secondary_yaxis('right', functions=(mtoHz, Hztom))
            else:
                secaxy = ax.secondary_yaxis('right', functions=(Hztom, mtoHz))
            secaxy.set_ylabel(ylabel_r)

        if savePlot:
            if filename is not None:
                if filename.find('.pdf') is not -1:
                    filename = filename.replace('.pdf', '')
            else:
                filename = self.Name+'_PSDPlot_'
            psdPlot.savefig(filename + time.strftime("%Y%m%d_%H%M%S") + '.pdf',
                            facecolor=psdPlot.get_facecolor(),
                            bbox_inches='tight')
        return psdPlot

    def savePSD(self, saveList=None, filename=None, doTotal=True,
                savePTfac=False):
        '''
        This function saves the PSDs from the PSDList into a csv file which
        can be used to load the PSDs again later.
        **Warning**: This function will work inly if all PSDs in saveList
                     share the same frequency axis.
        Input arguments:
            saveList: List of keys of self.PSDList to be saved. If None, only
                        PDS from self.calcPSDList will be saved.
            filename: If provided, data will be saved with this filename
            doTotal: If true, total expected noise is calculated and saved.
        '''

        if saveList is None:
            saveList = self.calcPSDList
        elif saveList == 'all':
            saveList = list(self.PSDList.keys())

        # Creating total expected noise
        if doTotal:
            self.calculateTotalEstNoise()
            if 'total' not in saveList:
                saveList += ['total']

        #
        if savePTfac:
            if 'PT_wo_RIN_coatAbs' not in saveList:
                saveList += ['PT_wo_RIN_coatAbs']

        # Calculating required number of columns to store data
        reqColLen = 1
        for key in saveList:
            if key in self.PSDList:
                if isinstance(self.PSDList[key][0][0],
                              unc.core.AffineScalarFunc):
                    reqColLen = reqColLen + 2
                elif len(self.PSDList[key]) == 5:
                    reqColLen = reqColLen + 3
                else:
                    reqColLen = reqColLen + 1
            else:
                print(key + ' has not been calculated yet.')

        # Initializing dataArr which will be written
        dataArr = np.zeros((len(self.freq), reqColLen))
        dataArr[:, 0] = self.freq
        colList = ['Freq (Hz)']

        # Add data to dataArr and create colList as well.
        ii = 1
        for key in saveList:
            if key in self.PSDList:
                print('Reading', key)
                start = time.time()
                if isinstance(self.PSDList[key][0][0],
                              unc.core.AffineScalarFunc):
                    dataArr[:, ii] = unpnv(self.PSDList[key][0])
                    colList += [key]
                    ii = ii + 1
                    dataArr[:, ii] = unpstd(self.PSDList[key][0])
                    colList += [key+'_std']
                    ii = ii + 1
                elif len(self.PSDList[key]) == 5:
                    dataArr[:, ii] = self.PSDList[key][0]
                    colList += [key]
                    ii = ii + 1
                    dataArr[:, ii] = self.PSDList[key][3]
                    colList += [key+'_lb']
                    ii = ii + 1
                    dataArr[:, ii] = self.PSDList[key][4]
                    colList += [key+'_ub']
                    ii = ii + 1
                else:
                    dataArr[:, ii] = self.PSDList[key][0]
                    colList += [key]
                    ii = ii + 1
                print('Took', time.time()-start, 's')

        # Create a dataframe object and write data
        tempdf = pd.DataFrame(dataArr, columns=colList)
        if filename is not None:
            if filename.find('.csv') is not -1:
                filename = filename.replace('.csv', '')
        else:
            filename = self.Name+'_PSDs_'
        tempdf.to_csv(filename + time.strftime("%Y%m%d_%H%M%S") + '.csv',
                      sep=',', index=False)

    def loadPSD(self, filename, overridePresentPSD=False,
                overridePresentFreq=False):
        '''
        This function loads the PSDs saved earlier using savePSD
        Input arguments:
            filename: filename of data file.
            overridePresentPSD: Whether read data from file should be
                                overwritten on present PSDs with same
                                key values. Defaults to False.
        '''
        try:
            tempdf = pd.read_csv(filename, sep=',', header=0)
        except BaseException:
            raise RuntimeError('Error in reading {fn}'.format(fn=filename))

        colList = list(tempdf.columns)

        # Convert dataframe to dict of ndarray
        tempdf = tempdf.to_dict('series')
        for col in tempdf:
            tempdf[col] = tempdf[col].to_numpy()

        # Change self.freq if asked
        if overridePresentFreq:
            self.freq = tempdf['Freq (Hz)']

        # Create loadList which is list of PSD names only
        loadList = []
        for col in colList:
            if all([col != 'Freq (Hz)', col.find('_std') == -1,
                    col.find('_lb') == -1, col.find('_ub') == -1]):
                loadList += [col]

        # Give full labels for following noise curves:
        fullLabel = {'coatBr': 'Coating Brownian Noise',
                     'coatTO': 'Coating Thermo-Optic Noise',
                     'subBr': 'Substrate Brownian Noise',
                     'subTE': 'Substrate Thermoelastic Noise',
                     'pdhShot': 'PDH Shot Noise',
                     'pllOsc': 'Moku Frequency Noise',
                     'pllReadout': 'Beatnote PD Dark Noise',
                     'seismic': 'Seismic Noise',
                     'photoThermal': 'Photothermal Noise',
                     'resNPRO': 'Residual NPRO Noise',
                     'total': 'Total Estimated Noise'}

        for psd in loadList:
            boundsPresent = False
            if psd + '_std' in colList:
                psdData = unp.uarray(tempdf[psd], tempdf[psd + '_std'])
            elif psd + '_lb' in colList:
                psdData = np.array(tempdf[psd])
                psdData_lb = np.array(tempdf[psd + '_lb'])
                psdData_ub = np.array(tempdf[psd + '_ub'])
                boundsPresent = True
                print('Found lower and upper bounds')
            else:
                psdData = np.array(tempdf[psd])
            if psd in self.PSDList:
                psdLabel = self.PSDList[psd][2]
                if overridePresentPSD:
                    if boundsPresent:
                        self.PSDList[psd] = [psdData, tempdf['Freq (Hz)'],
                                             psdLabel, psdData_lb, psdData_ub]
                    else:
                        self.PSDList[psd] = [psdData, tempdf['Freq (Hz)'],
                                             psdLabel]
                    print('Overwriting {key}'.format(key=psd))
                else:
                    print('Skipping {key}'.format(key=psd))
            else:
                if psd in fullLabel:
                    psdLabel = fullLabel[psd]
                else:
                    psdLabel = psd
                if boundsPresent:
                    self.PSDList[psd] = [psdData, tempdf['Freq (Hz)'],
                                         psdLabel, psdData_lb, psdData_ub]
                else:
                    self.PSDList[psd] = [psdData, tempdf['Freq (Hz)'],
                                         psdLabel]
                print('Adding {key}'.format(key=psd))

    def loadASD(self, spectrumFile, key='Beat', label='Beat'):
        '''
        This function loads any ASD spectrum file as PSD in the PSDList.
        Most common use is to load measured spectrum from SR785 or Moku
        '''
        beat = np.loadtxt(spectrumFile)
        if np.shape(beat)[1] == 2:
            self.PSDList[key] = [beat[:, 1]**2, beat[:, 0], label]
        elif np.shape(beat)[1] == 4:
            print('Found lower and upper bounds')
        self.PSDList[key] = [beat[:, 1]**2, beat[:, 0], label,
                             beat[:, 2]**2, beat[:, 3]**2]

    def showCode(self, funcName):
        '''
        This function prints out the code of the funcName function argument.
        Note: This function doesn't work for itself.
        '''
        with open("noiseBudgetModule.py", 'r') as nbm:
            allCode = nbm.readlines()
            onFunction = False
            for line in allCode:
                if onFunction:
                    if (('def ' in line) or ('#:::' in line)
                            or ('#***' in line) or ('class ' in line)):
                        onFunction = False
                    else:
                        print(line.replace('\n', ''))
                if 'def '+funcName in line:
                    onFunction = True
                    print(line.replace('\n', ''))

# *****************************************************************************


# *****************************************************************************
# Helper functions.
# *****************************************************************************
def uquad(func, a, b, args=(), full_output=0, epsabs=1.49e-8, epsrel=1.49e-8,
          limit=50, points=None, weight=None, wvar=None, wopts=None, maxp1=50,
          limlst=50):
    '''
    This is a wrapper for scipy.integrate.quad() to support integration
    of functions returning uncertainty.
    The standard deviation of the integrated output is calculated by
    suming the standard deviation of function values at discrete
    points in the range multiplied by the distance between them. Standard
    deviation is used instead of variance to assume maximum covariance
    between the function values at different points. This will always
    overestimate the standard deviation.
    This func will be most accurate for slowly varying functions and
    mot inaccurate for haphazardly varying functions.
    Test case:
    def integrand(x, a, b, c):
        return a*x**2 + b*x + c
    a = uf(1, 0.1)
    b = uf(1, 0.1)
    c = uf(1, 0.1)
    uquad(integrand, 0, 1, args=(a, b, c), limit=50000)
    output:
    (1.8333333333333335+/-0.1219739190960589, 2.0354088784794538e-14)
    Analytical Answer:
    (1.8333333333333335+/-0.1166666666666667)

    '''
    def NVfunc(*args, **kwargs):
        return func(*args, **kwargs).nominal_value
    integratedNV, intErrorNV = scint.quad(NVfunc, a, b, args=args,
                                          full_output=full_output,
                                          epsabs=epsabs, epsrel=epsrel,
                                          limit=limit, points=points,
                                          weight=weight, wvar=wvar,
                                          wopts=wopts, maxp1=maxp1,
                                          limlst=limlst)
    ul = max(a, b)
    ll = min(a, b)
    dx = (b-a)/limit
    xAxis = np.arange(ll, ul, dx)
    stdSum = 0
    for x in xAxis:
        stdSum = stdSum + (func(x, *args).std_dev*dx)
    return (uf(integratedNV, stdSum), intErrorNV)


# *****************************************************************************
def integrandM(uu, Omega):
    Omega = unpnv(Omega)
    return np.real(uu * np.exp(-(uu**2)/2) / np.sqrt(uu**2 - 1j*Omega))


# *****************************************************************************
def extractValue(valueStr):
    '''
    This function is to parse values loaded by yaml.full_load()
    If the value is loaded as float, it will be converted to ufloat with zero
    uncertainty.
    If the value is loaded as string with +/- operator, it will be converted
    to ufloat with the defined uncertainty.
    Valid inputs:
    1064.0
    '0.35+/-0.05'
    '(1.50+/-0.10)e+04'
    White spaces can be introduced anywhere. They will be ignored.
    '''
    if isinstance(valueStr, float):
        # Case for a float input
        return uf(valueStr, 0)
    elif valueStr.find('+/-') != -1:
        valsplit = valueStr.split('+/-')
        if '(' in valsplit[0]:
            valsplit[0] = valsplit[0].replace('(', '')
            expsplit = valsplit[1].split(')')
            valsplit[1] = expsplit[0]+expsplit[1]
            valsplit[0] = valsplit[0]+expsplit[1]
        value = float(valsplit[0].replace(' ', ''))
        uncer = float(valsplit[1].replace(' ', ''))
    else:
        # Case when uncertainity is not provided
        value = float(valueStr)
        uncer = 0.0
    return uf(value, uncer)


# *****************************************************************************
def fmtString(strList):
    '''
    This function joins a list of strings with adding white spaces in between
    the strings to ensure each string start at a particular position.
    Example:
    fmtString(['Cond:',11,str(self.Cond),32,'# W/(m*K), Heat Conductivity\n'])
    will output
    Cond:      55.0+/-3.0           # W/(m*K), Heat Conductivity
               ^11                  ^32
    '''
    returnString = ''
    for ele in strList:
        if isinstance(ele, str):
            returnString = returnString + ele
        elif isinstance(ele, int):
            padString = '{:<' + str(ele) + '}'
            returnString = padString.format(returnString)
        else:
            raise RuntimeError('List elements should be strings and integers'
                               ' only.')
    return returnString
# *****************************************************************************
# Setup for handling notebook/script python interpreter mode


def isnotebook():
    try:
        shell = get_ipython().__class__.__name__
        if shell == 'ZMQInteractiveShell':
            return True   # Jupyter notebook or qtconsole
        elif shell == 'TerminalInteractiveShell':
            return False  # Terminal running IPython
        else:
            return False  # Other type (?)
    except NameError:
        return False      # Probably standard Python interpreter
# *****************************************************************************
# Overloaded save and load for all objects with Name.
# *****************************************************************************


def save(obj):
    '''
    This function saves
    '''
    pickle.HIGHEST_PROTOCOL = 3
    pickle.DEFAULT_PROTOCOL = 2
    print('Saving object in file ', obj.Name, '.dat')
    with open(obj.Name+'.dat', 'wb') as f:
        pickle.dump(obj, f, 2)
        f.close()


def load(objFileName):
    '''
    This funciton creates an object saved earlier with save function.
    '''
    pickle.HIGHEST_PROTOCOL = 3
    pickle.DEFAULT_PROTOCOL = 2
    with open(objFileName, 'rb') as f:
        rtnobj = pickle.Unpickler(f).load()
        f.close()
        return rtnobj
