# Helpful class for uncertainties in complex values.
from uncertainties import ufloat as uf
from uncertainties import unumpy as unp


class ucomplex:
    def __init__(self, x=None, y=uf(0, 0), mag=None, ang=uf(0, 0)):
        if mag is None:
            if x is not None:
                self.x = x*uf(1, 0)
                self.y = y*uf(1, 0)
            else:
                self.x = uf(0, 0)
                self.y = uf(0, 0)
        else:
            self.x = mag*unp.cos(ang)*uf(1, 0)
            self.y = mag*unp.sin(ang)*uf(1, 0)

    def __repr__(self):
        return '('+repr(self.x)+') + ('+repr(self.y) + ')*1j'

    def __str__(self):
        return '('+repr(self.x)+') + ('+repr(self.y) + ')*1j'

    def abs(self):
        return unp.sqrt(self.x**2 + self.y**2) + uf(0, 0)

    def ang(self):
        return unp.arctan(self.y/self.x) + uf(0, 0)

    @property
    def imag(self):
        return self.y

    @property
    def real(self):
        return self.x

    @property
    def nominal_value(self):
        return self.x.nominal_value + 1j*self.y.nominal_value

    @property
    def n(self):
        return self.nominal_value

    @property
    def std_dev(self):
        return self.x.std_dev + 1j*self.y.std_dev

    @property
    def s(self):
        return self.std_dev

    def __neg__(self):
        return ucomplex(x=-self.x, y=-self.y)

    def __add__(self, arg2):
        if isinstance(arg2, ucomplex):
            return ucomplex(x=self.x+arg2.x, y=self.y+arg2.y)
        else:
            return ucomplex(x=self.x + arg2, y=self.y)

    def __radd__(self, arg2):
        return self + arg2

    def __sub__(self, arg2):
        if isinstance(arg2, ucomplex):
            return ucomplex(x=self.x-arg2.x, y=self.y-arg2.y)
        else:
            return ucomplex(x=self.x - arg2, y=self.y)

    def __rsub__(self, arg2):
        return -self + arg2

    def __mul__(self, arg2):
        if isinstance(arg2, ucomplex):
            return ucomplex(x=self.x*arg2.x - self.y*arg2.y,
                            y=self.x*arg2.y + self.y*arg2.x)
        else:
            return ucomplex(x=self.x*arg2, y=self.y*arg2)

    def __rmul__(self, arg2):
        return self*arg2

    def __pow__(self, arg2):
        if isinstance(arg2, int):
            retVal = ucomplex(1)
            for ii in range(arg2):
                retVal = ucomplex(x=self.x, y=self.y)*retVal
            return retVal
        else:
            raise RuntimeError('ucomplex can be raised to positive integral'
                               ' powers only.')

    def __truediv__(self, arg2):
        if isinstance(arg2, ucomplex):
            return ucomplex(x=(self.x*arg2.x + self.y*arg2.y)/(arg2.abs()**2),
                            y=(-self.x*arg2.y + self.y*arg2.x)/(arg2.abs()**2))
        else:
            return ucomplex(x=self.x/arg2, y=self.y/arg2)

    def __rtruediv__(self, arg2):
        if isinstance(arg2, ucomplex):
            return ucomplex(x=(arg2.x*self.x + arg2.y*self.y)/(self.abs()**2),
                            y=(-arg2.x*self.y + arg2.y*self.x)/(self.abs()**2))
        else:
            return ucomplex(x=arg2*self.x/(self.abs()**2),
                            y=-arg2*self.y/(self.abs()**2))

# *****************************************************************************
